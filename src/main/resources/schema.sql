drop table account cascade ;
drop  table chat cascade ;
drop table product cascade ;
drop table message cascade ;
drop table account_product cascade ;
-- drop table feedback cascade ;--это может вообще удалю
drop type status;
create type status as enum ('simple_account', 'professional_account', 'unknown');

create table account(

    id bigserial primary key,

--вообще она должна быть уникальной,
-- но тут чтобы не создавать 100500 новых почт она не уникальна
    email varchar(100),

--     password varchar(20),
    first_name varchar(20),
    last_name varchar(20),
    age integer,
    file_id bigint,
    foreign key (file_id) references file_info(id),
    role status default 'unknown',
    salt bytea,
    password_hash bytea,
    random_code_for_activation varchar(1000)

);


alter table account add column file_id bigint;






-- alter table account add salt bytea;
-- alter table account add password_hash bytea;

-- drop type  pr_type;
-- create type pr_type as enum ('products_for_home', 'furniture', 'adornments', 'electronic', '') ;


create table product(
    id bigserial primary key,
    name varchar(30),
    description varchar(100),
    account_id bigint,
    product_type varchar(20),
    date date,
    count bigint,
--     photo varchar default '',
    price bigint,
    foreign key (account_id) references account(id)--это пользователь который продает товар
);






--покупкa айди челика который покупает и айди товара
create table account_product(
    account_id bigint,
    foreign key (account_id) references account(id),
    product_id bigint,
    foreign key (product_id) references product(id)
);
-- drop table feedback;
-- create table feedback(
--     account_id bigint,
--     foreign key (account_id)references account(id),
--     text varchar (1000)
--
-- );

--sale продажа
--purchase покупка
--create type sale_or_purchase as enum ('sale', 'purchase');

create table chat(
                     id bigserial primary key,
                     first_account_id bigint,
                     foreign key (first_account_id) references account(id),
                     second_account_id bigint,
                     foreign key (second_account_id) references account(id)
);

create table message(
    text varchar(100),
    chat_id bigint,
    foreign key (chat_id) references chat(id),
    account_id bigint,
    foreign key (account_id) references account(id),
    time timestamp
);

create table product_photos(
    product_id bigint,
    foreign key (product_id) references product(id),
    photo_address varchar(100)--это адрес фотографии в моем приложении(путь до папки в которой лежит картинка)

);

drop table product_photos;




drop table file_info;

create table file_info(
    id bigserial primary key,
    original_file_name varchar(100),
    storage_file_name varchar(100),
    size bigint,
    mime_type varchar(30)
);


create table product_file_photo(
    product_id bigint,
    foreign key (product_id) references product(id),
    file_id bigint,
    foreign key (file_id) references file_info(id)

);





select product_id, storage_file_name from product_file_photo left join file_info fi on fi.id = product_file_photo.file_id where product_id =14;



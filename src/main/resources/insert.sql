INSERT INTO public.account (id, email, first_name, last_name, age, photo, role, salt) VALUES (10, 'ol@gmail.com', 'Oleg', 'Olegov', 20, '', 'unknown', E'\\x79CCB5CD75F14A7E9ADD3C9C8CBEE825');
INSERT INTO public.account (id, email, first_name, last_name, age, photo, role, salt) VALUES (6, 'a@gmail.com', 'Anton', 'Ant', 20, '', 'simple_account', E'\\xFA64D1B88C95386CBF8981A6B31B1A6F');
INSERT INTO public.account (id, email, first_name, last_name, age, photo, role, salt) VALUES (8, 'ale@gmail.com', 'Alex', 'Alexenov', 25, '', 'simple_account', E'\\x4CEA9C5A9FB78688D46B053D16F49581');
INSERT INTO public.account (id, email, first_name, last_name, age, photo, role, salt) VALUES (9, 'i@gmail.com', 'Iv', 'Ivankov', 20, '', 'simple_account', E'\\xBC944BD511DCAB8AE04D9E6B7288B128');
INSERT INTO public.account (id, email, first_name, last_name, age, photo, role, salt) VALUES (4, 'm@gmail.com', 'Maria', 'Ivanova', 20, '', 'simple_account', E'\\xF8CD4CA3E630C8C1526339E7AECBCBB4');
INSERT INTO public.account (id, email, first_name, last_name, age, photo, role, salt) VALUES (7, 't@gmail.com', 'Tana', 'Tatanova', 20, '', 'simple_account', E'\\x9B969C568A3AF4532A99AFFE22BA1FF4');
INSERT INTO public.account (id, email, first_name, last_name, age, photo, role, salt) VALUES (5, 'e@gmail.com', 'Ev', 'Evikov', 20, '', 'simple_account', E'\\x0763B69F4B035555CBE1E7B93DAA3D9F');
INSERT INTO public.account (id, email, first_name, last_name, age, photo, role, salt) VALUES (3, 'petr@gmail.com', 'Petr', 'Pol', 20, '', 'simple_account', E'\\x47B08B34E73274FA1BE0A7B98E1F4E64');


INSERT INTO public.product (id, name, description, account_id, product_type, date, count, photo, price) VALUES (2, 'Laptop', 'Coll laptop for everyone', 8, 'Electronic', '2022-10-16', 2, '', 60000);
INSERT INTO public.product (id, name, description, account_id, product_type, date, count, photo, price) VALUES (3, 'Phone', 'New phone, use just 1 week', 8, 'Electronic', '2022-10-16', 1, '', 20000);
INSERT INTO public.product (id, name, description, account_id, product_type, date, count, photo, price) VALUES (4, 'Laptop', 'Use 1 year', 9, 'Electronic', '2022-10-16', 1, '', 50000);
INSERT INTO public.product (id, name, description, account_id, product_type, date, count, photo, price) VALUES (5, 'Table', 'Bay one week ago', 9, 'products_for_home', '2022-10-16', 2, '', 3000);
INSERT INTO public.product (id, name, description, account_id, product_type, date, count, photo, price) VALUES (6, 'Lamp', 'Very nace lamp', 9, 'products_for_home', '2022-10-16', 5, '', 1000);
INSERT INTO public.product (id, name, description, account_id, product_type, date, count, photo, price) VALUES (7, 'Ring', 'I don''t believe in love, my fiancee left me, so I will sell the ring urgently', 3, 'adornments', '2022-10-16', 1, '', 5000);


delete from product where account_id = 6;


INSERT INTO public.account (id, email, first_name, last_name, age, role, salt, password_hash, random_code_for_activation, file_id) VALUES (1, 'examplefor575@mail.ru', 'Ann', 'Appa', 20, 'professional_account', E'\\xDEA2FE12991F17D12731B427718E2EFA', E'\\x98B984612941A73CD625A1157ADBBA08', '0593cde4-5950-41e1-a8b8-ae9626aa4822', null);
INSERT INTO public.account (id, email, first_name, last_name, age, role, salt, password_hash, random_code_for_activation, file_id) VALUES (10, 'ol@gmail.com', 'Oleg', 'Olegov', 20, 'unknown', E'\\x79CCB5CD75F14A7E9ADD3C9C8CBEE825', E'\\x983A3760B6EB436CAD00DAAD335B83EB', 'cfb78931-290f-4abb-a89c-b48b2a40e9d7', null);
INSERT INTO public.account (id, email, first_name, last_name, age, role, salt, password_hash, random_code_for_activation, file_id) VALUES (6, 'a@gmail.com', 'Anton', 'Ant', 20, 'simple_account', E'\\xFA64D1B88C95386CBF8981A6B31B1A6F', E'\\x7910C89F398C7B712B8C0B108F48755E', '1b8077bc-87a7-460c-9bac-5ff3a1e3b8ed', null);
INSERT INTO public.account (id, email, first_name, last_name, age, role, salt, password_hash, random_code_for_activation, file_id) VALUES (4, 'm@gmail.com', 'Maria', 'Ivanova', 20, 'simple_account', E'\\xF8CD4CA3E630C8C1526339E7AECBCBB4', E'\\xBE1FBF5AF953F427F2828DB598243F64', 'b4daf005-5426-4e3a-a717-28df9149cd6f', null);
INSERT INTO public.account (id, email, first_name, last_name, age, role, salt, password_hash, random_code_for_activation, file_id) VALUES (7, 't@gmail.com', 'Tana', 'Tatanova', 20, 'simple_account', E'\\x9B969C568A3AF4532A99AFFE22BA1FF4', E'\\x4F35A2FFE819FB061D11FDD701717DBA', '9a1994cc-7893-49e1-8ffa-7bb3e302f0e0', null);
INSERT INTO public.account (id, email, first_name, last_name, age, role, salt, password_hash, random_code_for_activation, file_id) VALUES (9, 'i@gmail.com', 'Iv', 'Ivankov', 20, 'professional_account', E'\\xBC944BD511DCAB8AE04D9E6B7288B128', E'\\x5B4D8A574EC3C4F9502D78641A7FD839', '2d9c6e76-bdcc-4175-a802-a7fac30624fc', null);
INSERT INTO public.account (id, email, first_name, last_name, age, role, salt, password_hash, random_code_for_activation, file_id) VALUES (3, 'petr@gmail.com', 'Petr', 'Pol', 20, 'professional_account', E'\\x47B08B34E73274FA1BE0A7B98E1F4E64', E'\\x7AAB1C261807A41B3AA53C2C3F1A9436', 'd0ae5ce8-68cf-40df-a166-407315b94588', null);
INSERT INTO public.account (id, email, first_name, last_name, age, role, salt, password_hash, random_code_for_activation, file_id) VALUES (2, 'poly@gmail.com', 'p', 'Pol', 12, 'professional_account', E'\\xD228930D3AC101803FEA714E4EF56A88', E'\\x832E3B106EEDF21CA9BF46535278A3E6', '068f32a9-a942-4cb5-94f1-983f34d18185', null);

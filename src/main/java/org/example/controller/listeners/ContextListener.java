package org.example.controller.listeners;

import org.example.model.connection.ConnectionToDataBase;
import org.example.model.repositories.*;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import org.example.model.repositories.impl.AccountsRepositoryImpl;
import org.example.model.repositories.impl.ChatsRepositoryImpl;
import org.example.model.repositories.impl.FileInfoRepositoryImpl;
import org.example.model.repositories.impl.ProductsRepositoryImpl;
import org.example.model.utils.Constants;


import java.nio.file.Path;

@WebListener
public class ContextListener implements ServletContextListener {

    private static AccountsRepository accountsRepository;
    private static ProductsRepository productsRepository;
    private static ChatsRepository chatsRepository;

    private static FileInfoRepository fileInfoRepository;

    private static final String STORAGE_PATH = Constants.STORAGE_PATH;

    @Override
    public void contextInitialized(ServletContextEvent sce) {


        System.out.println("contextListener");

        String tomcatBase = System.getProperty("catalina.base");

        System.out.println(tomcatBase);
        accountsRepository = new AccountsRepositoryImpl(ConnectionToDataBase.connection());
        productsRepository = new ProductsRepositoryImpl(ConnectionToDataBase.connection());
        chatsRepository = new ChatsRepositoryImpl(ConnectionToDataBase.connection());
        fileInfoRepository = new FileInfoRepositoryImpl(ConnectionToDataBase.connection());



        sce.getServletContext().setAttribute("accountsRepository", accountsRepository);
        sce.getServletContext().setAttribute("productsRepository", productsRepository);
        sce.getServletContext().setAttribute("chatsRepository", chatsRepository);
        sce.getServletContext().setAttribute("fileInfoRepository", fileInfoRepository);



        sce.getServletContext().setAttribute("STORAGE_PATH", STORAGE_PATH);



    }


}

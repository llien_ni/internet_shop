package org.example.controller.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.models.Account;
import org.example.model.repositories.AccountsRepository;
import org.example.model.services.AccountService;
import org.example.model.services.impl.AccountServiceImpl;
import org.example.model.utils.PasswordSecurity;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {


    private AccountService accountService;


    @Override
    public void init() throws ServletException {

        accountService = new AccountServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String error = "";
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        if (!accountService.isEmailAlreadyExist(email)) {
            error = "Пользователя с таким email не существует";
            req.setAttribute("error", error);
            req.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(req, resp);
        }

        byte[] saltForEmail = accountService.getSaltForEmail(email);

        byte[] passwordHashForEmail = accountService.getPasswordHash(email);

        byte[] calculateForThisPassword = PasswordSecurity.calculateHash(password, saltForEmail);

        if (Arrays.equals(passwordHashForEmail, calculateForThisPassword)) {
            System.out.println("Cool!");

            Account account = accountService.findByEmailAndPassword(email, passwordHashForEmail);
            req.getSession().setAttribute("account", account);

            resp.sendRedirect("/home_page");


        } else {
            System.out.println("bad");

            error = "Неверный пароль";

            req.setAttribute("error", error);
            req.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(req, resp);


        }

    }
}

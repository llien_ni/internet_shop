package org.example.controller.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.dto.AccountDto;
import org.example.model.dto.FileDto;
import org.example.model.models.Account;
import org.example.model.repositories.AccountsRepository;
import org.example.model.repositories.FileInfoRepository;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.AccountPhotosService;
import org.example.model.services.AccountService;
import org.example.model.services.FilesService;
import org.example.model.services.impl.AccountPhotosServiceImpl;
import org.example.model.services.impl.AccountServiceImpl;
import org.example.model.services.ProductService;
import org.example.model.services.impl.FilesServiceImpl;
import org.example.model.services.impl.ProductServiceImpl;

import java.io.IOException;


@WebServlet("/producer")
public class ProducerServlet extends HttpServlet {

    private AccountService accountService;
    private FilesService filesService;
    private AccountPhotosService accountPhotosService;
    private String STORAGE = "/storage/";

    @Override
    public void init() throws ServletException {

        accountService = new AccountServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));

        accountPhotosService = new AccountPhotosServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));

        filesService = new FilesServiceImpl((FileInfoRepository) getServletContext().getAttribute("fileInfoRepository"));

//        filesService.setStoragePath((String) getServletContext().getAttribute("storagePath"));

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long producerId = Long.parseLong(req.getParameter("producer_id"));
        Account producer = accountService.findById(producerId);

        AccountDto accountDto = accountPhotosService.addPreviewPhotoIfExist(producer);

        req.setAttribute("producer", accountDto);


        req.getRequestDispatcher("/WEB-INF/jsp/producer.jsp").forward(req, resp);
    }
}

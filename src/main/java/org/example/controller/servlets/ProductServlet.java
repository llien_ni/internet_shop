package org.example.controller.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.dto.FileDto;
import org.example.model.dto.ProductDto;
import org.example.model.models.Account;
import org.example.model.models.Product;
import org.example.model.repositories.AccountsRepository;
import org.example.model.repositories.FileInfoRepository;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.AccountService;
import org.example.model.services.FilesService;
import org.example.model.services.ProductPhotosService;
import org.example.model.services.impl.AccountServiceImpl;
import org.example.model.services.ProductService;
import org.example.model.services.impl.FilesServiceImpl;
import org.example.model.services.impl.ProductPhotosServiceImpl;
import org.example.model.services.impl.ProductServiceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@WebServlet("/this_product")
public class ProductServlet extends HttpServlet {
    private ProductService productService;
    private AccountService accountService;

    private ProductPhotosService productPhotosService;


    @Override
    public void init() throws ServletException {
        productService = new ProductServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));

        accountService = new AccountServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));

        productPhotosService = new ProductPhotosServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Long productId = Long.valueOf(req.getParameter("id"));

        //нашли продукт по id
        Product productFound = productService.findById(productId);

        //по продукту нашли его производителя
        Long producerId = productFound.getAccountId();

        //по id аккаунта нашли сам аккаунт
        Account account = accountService.findById(producerId);
        String accountFirstName = account.getFirstName();
        String accountLastName = account.getLastName();


        ProductDto productDto = productPhotosService.addPicturesIfExists(productFound);



        req.setAttribute("accountFirstName", accountFirstName);
        req.setAttribute("accountLastName", accountLastName);
        req.setAttribute("product", productDto);



        req.getRequestDispatcher("/WEB-INF/jsp/product/product.jsp").forward(req, resp);


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher("/WEB-INF/jsp/product/product.jsp").forward(req, resp);


    }


}
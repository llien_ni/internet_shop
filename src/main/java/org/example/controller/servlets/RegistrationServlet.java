package org.example.controller.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
//import org.example.controller.filters.AuthenticationFilter;
import org.example.model.dto.AccountForm;
import org.example.model.models.Account;
import org.example.model.repositories.AccountsRepository;
import org.example.model.services.AccountService;
import org.example.model.services.impl.AccountServiceImpl;
import org.example.model.utils.EmailUtils;

import java.io.IOException;

@WebServlet("/reg")
public class RegistrationServlet extends HttpServlet {

    private AccountService accountService;

    @Override
    public void init() throws ServletException {
        accountService = new AccountServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String error = "";

        String email = req.getParameter("email");

        String firstName = req.getParameter("first_name");
        String lastName = req.getParameter("last_name");
        String password = req.getParameter("password");
        Integer age = Integer.valueOf(req.getParameter("age"));

//        if(email == null || password == null){
//            error = "Email or password can't be empty";
//            req.setAttribute("error", error);
//            req.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(req, resp);
//            return;
//        }

        if(accountService.isEmailAlreadyExist(email)){
            error = "Пользователь с такой почтой уже зарегистрирован.";
            req.setAttribute("error", error);
            req.getRequestDispatcher("WEB-INF/jsp/registration.jsp").forward(req, resp);
            return;
        }

        AccountForm form = new AccountForm(firstName, lastName, age, email, password);



        accountService.signUp(form);//тут он сохраняется в базу и надо убрать присваивание ему поля simple account
//simple account должен присваиваться когда челик заходит на почту и нажимает активировать




        byte[] passwordHash = accountService.getPasswordHash(email);

        Account account = accountService.findByEmailAndPassword(email,passwordHash);
        //делаем отправку сообщения для подтверждения пользователю
        EmailUtils.sendAccountActivateEmail(account);

        req.getSession().setAttribute("account", account);


        resp.sendRedirect("/home_page");


    }
}

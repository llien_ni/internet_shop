package org.example.controller.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.dto.ProductDto;
import org.example.model.models.Product;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.ProductPhotosService;
import org.example.model.services.ProductService;
import org.example.model.services.impl.ProductPhotosServiceImpl;
import org.example.model.services.impl.ProductServiceImpl;

import java.io.*;
import java.util.List;

@WebServlet(urlPatterns = {"", "/home_page"})
public class HomePageServlet extends HttpServlet {
    private ProductService productService;
    private ProductPhotosService productPhotosService;



    @Override
    public void init() throws ServletException {
        productService = new ProductServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));

        productPhotosService = new ProductPhotosServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));




    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Product> products = productService.findAll();
        List<ProductDto> allProducts = productPhotosService.addPreviewPictureIfExist(products);

        req.setAttribute("allProducts", allProducts);


        String findByName = req.getParameter("find");
        if(findByName!= null){
            products = productService.findByName(findByName);


            req.setAttribute("allProducts", productPhotosService.addPreviewPictureIfExist(products));
            req.getRequestDispatcher("/WEB-INF/jsp/home_page.jsp").forward(req, resp);
            return;
        }

        String filter = req.getParameter("filter");
        if(filter != null){
            products = productService.findWhereType(filter);
            req.setAttribute("allProducts", productPhotosService.addPreviewPictureIfExist(products));
            req.getRequestDispatcher("/WEB-INF/jsp/home_page.jsp").forward(req, resp);
            return;
        }

        req.getRequestDispatcher("/WEB-INF/jsp/home_page.jsp").forward(req, resp);


    }


}

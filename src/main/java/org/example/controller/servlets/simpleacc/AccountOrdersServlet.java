package org.example.controller.servlets.simpleacc;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.dto.ProductDto;
import org.example.model.models.Account;
import org.example.model.models.Product;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.ProductPhotosService;
import org.example.model.services.ProductService;
import org.example.model.services.impl.ProductPhotosServiceImpl;
import org.example.model.services.impl.ProductServiceImpl;

import java.io.IOException;
import java.util.List;

@WebServlet("/account/orders")
public class AccountOrdersServlet extends HttpServlet {

    private ProductService productService;
    private ProductPhotosService productPhotosService;


    @Override
    public void init() throws ServletException {
        productService = new ProductServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));
        productPhotosService = new ProductPhotosServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));

    }



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Account account = (Account) req.getSession().getAttribute("account");
        List<Product> products = productService.findProductsBoughtAccount(account.getId());

        List<ProductDto> productsBoughtAccount = productPhotosService.addPreviewPictureIfExist(products);
//        req.setAttribute("productsAddedThisAccount", productsBoughtAccount);

        req.setAttribute("products", productsBoughtAccount);

        req.getRequestDispatcher("/WEB-INF/jsp/account/account_orders.jsp").forward(req, resp);
    }


}

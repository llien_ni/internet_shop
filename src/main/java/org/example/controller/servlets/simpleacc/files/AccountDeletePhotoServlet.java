package org.example.controller.servlets.simpleacc.files;


import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.dto.FileDto;
import org.example.model.models.Account;
import org.example.model.repositories.AccountsRepository;
import org.example.model.repositories.FileInfoRepository;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.AccountPhotosService;
import org.example.model.services.AccountService;
import org.example.model.services.FilesService;
import org.example.model.services.impl.AccountPhotosServiceImpl;
import org.example.model.services.impl.AccountServiceImpl;
import org.example.model.services.impl.FilesServiceImpl;
import org.example.model.services.impl.ProductPhotosServiceImpl;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/account/delete_account_photo")
public class AccountDeletePhotoServlet extends HttpServlet {
    private AccountService accountService;
    private FilesService filesService;
    private AccountPhotosService accountPhotosService;


    @Override
    public void init() throws ServletException {
        accountPhotosService = new AccountPhotosServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));

        accountService = new AccountServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));

        filesService = new FilesServiceImpl((FileInfoRepository) getServletContext().getAttribute("fileInfoRepository"));

        filesService.setStoragePath((String) getServletContext().getAttribute("STORAGE_PATH"));



    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long accountId = ((Account)req.getSession().getAttribute("account")).getId();



        String fileForAccount = accountPhotosService.getFileForAccount(accountId);
        accountPhotosService.deleteFileForAccount(accountId);
        filesService.deleteFile(filesService.getFile(fileForAccount));


        req.getSession().setAttribute("account", accountService.findById(accountId));
        resp.sendRedirect("/account/update_info?");


    }
}

package org.example.controller.servlets.simpleacc;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.dto.MessageDto;
import org.example.model.models.Account;
import org.example.model.models.Chat;
import org.example.model.models.Message;
import org.example.model.repositories.AccountsRepository;
import org.example.model.repositories.ChatsRepository;
import org.example.model.services.*;
import org.example.model.services.impl.AccountServiceImpl;
import org.example.model.services.impl.ChatServiceImpl;
import org.example.model.services.impl.MessagesServiceImpl;

import java.io.IOException;
import java.util.List;


@WebServlet("/account/chat")
public class ChatServlet extends HttpServlet {

    private ChatService chatService;
    private MessagesService messagesService;


    @Override
    public void init() throws ServletException {
        chatService = new ChatServiceImpl((ChatsRepository) getServletContext().getAttribute("chatsRepository"));

        AccountService accountService = new AccountServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));

        messagesService = new MessagesServiceImpl(accountService);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Long currentAccountId = ((Account) req.getSession().getAttribute("account")).getId();

        //это когда мы первый раз зашли в чат
        if(req.getParameter("producer_id") !=null){
            Long producerId = Long.parseLong(req.getParameter("producer_id"));

            if (!chatService.isChatForThisAccountsAlreadyExist(currentAccountId, producerId)) {
                chatService.createChat(currentAccountId, producerId);

            }
            Chat chat = chatService.findChatIdByThisAccountIdAndProducerId(currentAccountId, producerId).get();

            req.getSession().setAttribute("chat", chat);

        }

        Chat chat = (Chat) req.getSession().getAttribute("chat");

        List<Message> messages = chatService.getMessages(chat.getChatId());

        List<MessageDto> messagesDtos = messagesService.convertMessages(messages);

        req.setAttribute("messages", messagesDtos);

        req.getRequestDispatcher("/WEB-INF/jsp/chat.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Chat chat = (Chat) req.getSession().getAttribute("chat");
        Long currentAccountId = ((Account) req.getSession().getAttribute("account")).getId();


        if (req.getParameter("message") != null) {
            String message = req.getParameter("message");
            chatService.addMessageToChat(currentAccountId, chat.getChatId(), message);
        }
        List<Message> messages = chatService.getMessages(chat.getChatId());

        req.setAttribute("messages", messages);
        resp.sendRedirect("/account/chat");


    }
}

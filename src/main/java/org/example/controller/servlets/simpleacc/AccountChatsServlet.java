package org.example.controller.servlets.simpleacc;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.models.Account;
import org.example.model.models.Chat;
import org.example.model.repositories.AccountsRepository;
import org.example.model.repositories.ChatsRepository;
import org.example.model.services.*;
import org.example.model.services.impl.AccountServiceImpl;
import org.example.model.services.impl.ChatServiceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@WebServlet("/account/chats")
public class AccountChatsServlet extends HttpServlet {

    private AccountService accountService;
    private ChatService chatService;


    @Override
    public void init() throws ServletException {
        accountService = new AccountServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));
        chatService = new ChatServiceImpl((ChatsRepository) getServletContext().getAttribute("chatsRepository"));


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Account account = (Account) req.getSession().getAttribute("account");
        List<Chat> chats = chatService.findThisUserChats(account.getId());

        List<Account> otherAccountsWithThisHaveChat = new ArrayList<>();


        for (Chat chat: chats){
            if(!Objects.equals(chat.getFirstAccId(), account.getId())){
                Account otherAccount = accountService.findById(chat.getFirstAccId());
                otherAccountsWithThisHaveChat.add(otherAccount);


            }else{

                Account otherAccount = accountService.findById(chat.getSecondAccId());
                otherAccountsWithThisHaveChat.add(otherAccount);

            }

        }

        req.setAttribute("otherAccountsWithThisHaveChat", otherAccountsWithThisHaveChat);


        req.getRequestDispatcher("/WEB-INF/jsp/account/account_chats.jsp").forward(req, resp);
    }
}

package org.example.controller.servlets.simpleacc;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.dto.AccountDto;
import org.example.model.dto.AccountForm;
import org.example.model.dto.FileDto;
import org.example.model.models.Account;
import org.example.model.repositories.AccountsRepository;
import org.example.model.repositories.FileInfoRepository;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.AccountPhotosService;
import org.example.model.services.AccountService;
import org.example.model.services.FilesService;
import org.example.model.services.ProductPhotosService;
import org.example.model.services.impl.*;

import java.io.IOException;


@WebServlet("/account/update_info")
public class AccountUpdateInfoServlet extends HttpServlet {
    private AccountService accountService;
    private AccountPhotosService accountPhotosService;
    private FilesService filesService;






    @Override
    public void init() throws ServletException {
        accountService = new AccountServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));


        filesService = new FilesServiceImpl((FileInfoRepository) getServletContext().getAttribute("fileInfoRepository"));

        filesService.setStoragePath((String) getServletContext().getAttribute("STORAGE_PATH"));

        accountPhotosService = new AccountPhotosServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Account account = (Account) req.getSession().getAttribute("account");

        AccountDto accountDto = accountPhotosService.addPreviewPhotoIfExist(account);
        req.setAttribute("account", accountDto);
        req.getRequestDispatcher("/WEB-INF/jsp/account/account_update_info.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Account account = (Account) req.getSession().getAttribute("account");


        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        Integer age = Integer.valueOf(req.getParameter("age"));

        AccountForm form = AccountForm.builder()
                .firstName(firstName)
                .lastName(lastName)
                .age(age)
                .build();

        //обновили поля
        accountService.update(account.getId(), form);

        //поменяли аккаунт на обновленный
        req.getSession().setAttribute("account",accountService.findById(account.getId()));



        AccountDto accountDto = accountPhotosService.addPreviewPhotoIfExist(account);



        req.setAttribute("account", account);


       resp.sendRedirect("/account/update_info?");



    }




}



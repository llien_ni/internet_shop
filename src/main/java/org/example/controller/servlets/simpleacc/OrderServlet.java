package org.example.controller.servlets.simpleacc;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.models.Account;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.ProductService;
import org.example.model.services.impl.ProductServiceImpl;

import java.io.IOException;

@WebServlet("/order")
public class OrderServlet extends HttpServlet {
    private ProductService productService;


    @Override
    public void init() throws ServletException {
        productService = new ProductServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));


    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long productId = Long.valueOf(req.getParameter("product_id"));

        Account account = (Account) req.getSession().getAttribute("account");
        Long accountId = account.getId();

        productService.addProductToOrder(productId, accountId);

        req.getRequestDispatcher("WEB-INF/jsp/order.jsp").forward(req, resp);

    }
}

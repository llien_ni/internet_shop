package org.example.controller.servlets.simpleacc;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.dto.AccountDto;
import org.example.model.models.Account;
import org.example.model.repositories.AccountsRepository;
import org.example.model.repositories.FileInfoRepository;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.*;
import org.example.model.services.impl.*;

import java.io.IOException;

@WebServlet("/account")
public class AccountServlet extends HttpServlet {

    private AccountPhotosService accountPhotosService;
    private AccountService accountService;
    private FilesService filesService;
    private ProductService productService;

    private static final String PROFESSIONAL_ACCOUNT = "professional_account";


    @Override
    public void init() throws ServletException {
        accountService = new AccountServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));
        accountPhotosService = new AccountPhotosServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));

        productService = new ProductServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));
        filesService = new FilesServiceImpl((FileInfoRepository) getServletContext().getAttribute("fileInfoRepository"));

        filesService.setStoragePath((String) getServletContext().getAttribute("STORAGE_PATH"));



    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        if(req.getParameter("makeProfAcc") != null){

            Long id = Long.valueOf(req.getParameter("makeProfAcc"));
            accountService.addRole(id, PROFESSIONAL_ACCOUNT);
            Account account = accountService.findById(id);
            req.getSession().setAttribute("account", account);
        }


        if(req.getParameter("deleteAcc")!=null){
            Long id = Long.valueOf(req.getParameter("deleteAcc"));
            Long fileForAccount = accountPhotosService.getIdFileForAccount(id);
            accountPhotosService.deleteFileForAccount(id);

            filesService.deleteFile(filesService.getFile(fileForAccount));
            //Account account = (Account) req.getSession().getAttribute("account");
            //productService.deleteAllProductsAddedThisAccount(account.getId());
            accountService.delete(id);
            req.getSession().removeAttribute("account");
            resp.sendRedirect("/home_page");
            return;

        }




        Account account = (Account) req.getSession().getAttribute("account");
        AccountDto accountDto = accountPhotosService.addPreviewPhotoIfExist(account);





        req.setAttribute("account", accountDto);

        req.getRequestDispatcher("/WEB-INF/jsp/account/account.jsp").forward(req, resp);
    }
}

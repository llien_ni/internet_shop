package org.example.controller.servlets.simpleacc.files;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.example.model.dto.FileDto;
import org.example.model.models.Account;
import org.example.model.repositories.AccountsRepository;
import org.example.model.repositories.FileInfoRepository;
import org.example.model.services.AccountPhotosService;
import org.example.model.services.AccountService;
import org.example.model.services.FilesService;
import org.example.model.services.impl.AccountPhotosServiceImpl;
import org.example.model.services.impl.AccountServiceImpl;
import org.example.model.services.impl.FilesServiceImpl;

import java.io.IOException;


@WebServlet("/account/upload")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50) // 50MB
public class UploadAccountPhotoServlet extends HttpServlet {

    private FilesService filesService;
    private AccountPhotosService accountPhotosService;


    @Override
    public void init() throws ServletException {

        accountPhotosService = new AccountPhotosServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));


        filesService = new FilesServiceImpl((FileInfoRepository) getServletContext().getAttribute("fileInfoRepository"));
        filesService.setStoragePath((String) getServletContext().getAttribute("STORAGE_PATH"));

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Account account = (Account) req.getSession().getAttribute("account");


        Part part = req.getPart("file");


        FileDto form = FileDto.builder()
                .originalFileName(part.getSubmittedFileName())
                .size(part.getSize())
                .mimeType(part.getContentType())
                .fileStream(part.getInputStream())
                .build();

        Long fileId = filesService.upload(form);
        accountPhotosService.addFileToAccount(account.getId(), fileId);

        resp.sendRedirect("/account/update_info?");

    }

}






package org.example.controller.servlets.proffacc;


import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.dto.FileDto;
import org.example.model.dto.ProductAddForm;
import org.example.model.dto.ProductDto;
import org.example.model.models.Product;
import org.example.model.repositories.FileInfoRepository;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.*;
import org.example.model.services.impl.FilesServiceImpl;
import org.example.model.services.impl.ProductPhotosServiceImpl;
import org.example.model.services.impl.ProductServiceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/account/update_product")
public class UpdateProductServlet extends HttpServlet {
    private ProductService productService;
    private ProductPhotosService productPhotosService;
    private String STORAGE_PATH;



    @Override
    public void init() throws ServletException {

        productService = new ProductServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));
        productPhotosService = new ProductPhotosServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));


        STORAGE_PATH = (String) getServletContext().getAttribute("STORAGE_PATH");

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long productId = Long.parseLong(req.getParameter("id"));
        Product product = productService.findById(productId);
        ProductDto productDto = ProductDto.from(product);

        List<String> photosAddresses = productPhotosService.getFilesForProduct(productId);

        if(photosAddresses != null){
            req.setAttribute("photosAddresses", photosAddresses);

        }


        req.setAttribute("product", productDto);

        req.getRequestDispatcher("/WEB-INF/jsp/product/update_product.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long productId = Long.parseLong(req.getParameter("id"));
        String name = req.getParameter("name");
        String description = req.getParameter("description");
        Integer count = Integer.parseInt(req.getParameter("count"));
        String productType = req.getParameter("product_type");
        Integer price = Integer.valueOf(req.getParameter("price"));


        ProductAddForm form = new ProductAddForm(name, description, count, productType, price);
        productService.update(form, productId);

        Product product = productService.findById(productId);


        ProductDto productDto = ProductDto.from(product);


        List<String> photosAddresses = productPhotosService.getFilesForProduct(productId);

        if (photosAddresses != null) {
            req.setAttribute("photosAddresses", photosAddresses);
        }

        req.setAttribute("product", productDto);
        resp.sendRedirect("/account/update_product?id=" + productId);

    }

}

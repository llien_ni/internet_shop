package org.example.controller.servlets.proffacc;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.repositories.FileInfoRepository;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.*;
import org.example.model.services.impl.DeletePhotoServiceImpl;
import org.example.model.services.impl.FilesServiceImpl;
import org.example.model.services.impl.ProductPhotosServiceImpl;
import org.example.model.services.impl.ProductServiceImpl;

import java.io.IOException;

@WebServlet("/account/delete_product")
public class DeleteProductServlet extends HttpServlet {
    private ProductService productService;
    private DeletePhotoService deletePhotoService;



    @Override
    public void init() throws ServletException {
        productService = new ProductServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));

        FilesService filesService = new FilesServiceImpl((FileInfoRepository) getServletContext().getAttribute("fileInfoRepository"));
        filesService.setStoragePath((String) getServletContext().getAttribute("STORAGE_PATH"));
        ProductPhotosService productPhotosService = new ProductPhotosServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));

        deletePhotoService = new DeletePhotoServiceImpl(productPhotosService, filesService);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long productId = Long.parseLong(req.getParameter("deleteId"));

        if(productId != null){
            //удалили связь
            deletePhotoService.deletePhotos(productId);
            //удалили сам продукт
            productService.delete(productId);

        }

       resp.sendRedirect("/account/products");
    }
}

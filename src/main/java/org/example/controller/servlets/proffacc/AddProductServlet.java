package org.example.controller.servlets.proffacc;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.example.model.dto.FileDto;
import org.example.model.dto.ProductAddForm;
import org.example.model.models.Account;
import org.example.model.repositories.FileInfoRepository;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.*;
import org.example.model.services.impl.FilesServiceImpl;
import org.example.model.services.impl.ProductPhotosServiceImpl;
import org.example.model.services.impl.ProductServiceImpl;


import java.io.IOException;
import java.util.Collection;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50) // 50MB
@WebServlet("/account/add_product")
public class AddProductServlet extends HttpServlet {

    private ProductService productService;
    private FilesService filesService;
    private ProductPhotosService productPhotosService;


    @Override
    public void init() throws ServletException {

        productService = new ProductServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));
        filesService = new FilesServiceImpl((FileInfoRepository) getServletContext().getAttribute("fileInfoRepository"));
        filesService.setStoragePath((String) getServletContext().getAttribute("STORAGE_PATH"));
        productPhotosService = new ProductPhotosServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/product/add_product.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Account account = (Account) req.getSession().getAttribute("account");

        String name = req.getParameter("name");
        String description = req.getParameter("description");
        Integer count = Integer.valueOf(req.getParameter("count"));
        Integer price = Integer.valueOf(req.getParameter("price"));
        String productType = req.getParameter("product_type");

        ProductAddForm form = ProductAddForm.builder()
                .name(name)
                .description(description)
                .count(count)
                .price(price)
                .productType(productType)
                .build();

        Long productId = productService.addProduct(form, account.getId());


        Collection<Part> parts = req.getParts();

        System.out.println(parts);
        for (Part filePart : parts) {

            if (filePart.getSubmittedFileName()!=null && filePart.getSubmittedFileName().length() != 0) {


                FileDto fileDto = FileDto.builder()
                        //это то имя с которым загрузил пользователь
                        .originalFileName(filePart.getSubmittedFileName())
                        .size(filePart.getSize())
                        .mimeType(filePart.getContentType())
                        .fileStream(filePart.getInputStream())
                        .build();

                if (fileDto != null) {

                    Long fileId = filesService.upload(fileDto);
                    productPhotosService.addFileToProduct(productId, fileId);

                }


            }

        }


        resp.sendRedirect("/account/products");
    }
}

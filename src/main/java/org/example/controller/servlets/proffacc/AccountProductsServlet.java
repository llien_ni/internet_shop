package org.example.controller.servlets.proffacc;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.dto.ProductDto;
import org.example.model.models.Account;
import org.example.model.models.Product;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.ProductPhotosService;
import org.example.model.services.ProductService;
import org.example.model.services.impl.ProductPhotosServiceImpl;
import org.example.model.services.impl.ProductServiceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@WebServlet("/account/products")
public class AccountProductsServlet extends HttpServlet {

    private ProductService productService;
    private ProductPhotosService productPhotosService;




    @Override
    public void init() throws ServletException {
        productService = new ProductServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));

        productPhotosService = new ProductPhotosServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Account account = (Account) req.getSession().getAttribute("account");
        List<Product> products = productService.findProductsAddedThisAccount(account.getId());

        List<ProductDto> productsAddedThisAccount = productPhotosService.addPreviewPictureIfExist(products);
        req.setAttribute("productsAddedThisAccount", productsAddedThisAccount);

        req.getRequestDispatcher("/WEB-INF/jsp/account/account_added_products.jsp").forward(req, resp);

    }





}

package org.example.controller.servlets.proffacc.files;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.example.model.dto.FileDto;
import org.example.model.repositories.FileInfoRepository;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.*;
import org.example.model.services.impl.FilesServiceImpl;
import org.example.model.services.impl.ProductPhotosServiceImpl;

import java.io.IOException;
import java.util.Collection;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50) // 50MB
@WebServlet("/upload_photo")
public class UploadProductPhotosServlet extends HttpServlet {
    private FilesService filesService;
    private ProductPhotosService productPhotosService;

    @Override
    public void init() throws ServletException {

        filesService = new FilesServiceImpl((FileInfoRepository) getServletContext().getAttribute("fileInfoRepository"));


        filesService.setStoragePath((String) getServletContext().getAttribute("STORAGE_PATH"));
        productPhotosService = new ProductPhotosServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/product/update_product.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Long productId = Long.parseLong(req.getParameter("product_id"));


        Collection<Part> parts = req.getParts();
        for(Part filePart: parts){

            if(filePart.getSubmittedFileName() != null) {


                System.out.println(filePart.getSubmittedFileName());

                FileDto form = FileDto.builder()
                        //это то имя с которым загрузил пользователь
                        .originalFileName(filePart.getSubmittedFileName())
                        .size(filePart.getSize())
                        //это типа такой фигни image/jpeg типа тип
                        // файла который отправили
                        .mimeType(filePart.getContentType())
                        //это сам стрим с файлом
                        .fileStream(filePart.getInputStream())
                        .build();

                Long fileId = filesService.upload(form);
                productPhotosService.addFileToProduct(productId, fileId);


            }


        }


        resp.sendRedirect("/account/update_product?id="+productId);



    }
}

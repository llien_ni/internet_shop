package org.example.controller.servlets.proffacc.files;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.repositories.FileInfoRepository;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.*;
import org.example.model.services.impl.DeletePhotoServiceImpl;
import org.example.model.services.impl.FilesServiceImpl;
import org.example.model.services.impl.ProductPhotosServiceImpl;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet("/delete_product_photo")
public class DeleteProductPhotoServlet extends HttpServlet {

    private DeletePhotoService deletePhotoService;

    @Override
    public void init() throws ServletException {

        FilesService filesService = new FilesServiceImpl((FileInfoRepository) getServletContext().getAttribute("fileInfoRepository"));
        filesService.setStoragePath((String) getServletContext().getAttribute("STORAGE_PATH"));
        ProductPhotosService productPhotosService = new ProductPhotosServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));

        deletePhotoService = new DeletePhotoServiceImpl(productPhotosService, filesService);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long productId = Long.parseLong(req.getParameter("productId"));
        String[] deleteFiles = req.getParameterValues("deletePhotoName");

        List<String> list = Arrays.asList(deleteFiles);
        System.out.println("from delete photo");
        System.out.println(list);


        if(!list.isEmpty()){
            deletePhotoService.deletePhoto(productId, list);


        }
        resp.sendRedirect("/account/update_product?id=" + productId);

    }



}

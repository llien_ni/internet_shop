package org.example.controller.servlets;


//import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.dto.ProductDto;
import org.example.model.models.Product;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.ProductPhotosService;
import org.example.model.services.ProductService;
import org.example.model.services.impl.ProductPhotosServiceImpl;
import org.example.model.services.impl.ProductServiceImpl;

import java.io.IOException;
import java.util.List;

//@WebServlet("/home_page/find")
public class HomePageJsonServlet extends HttpServlet {

    private ProductService productService;
    private ProductPhotosService productPhotosService;

//    private ObjectMapper objectMapper;


    @Override
    public void init() throws ServletException {
        productService = new ProductServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));

        productPhotosService = new ProductPhotosServiceImpl((ProductsRepository) getServletContext().getAttribute("productsRepository"));

//        objectMapper = new ObjectMapper();



    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Product> products = productService.findByName(req.getParameter("findByName"));

        List<ProductDto> productDtos = productPhotosService.addPreviewPictureIfExist(products);


        resp.setContentType("application/json");
//        String json = objectMapper.writeValueAsString(productDtos);
//        resp.getWriter().println(json);


        //req.setAttribute("allProducts", productPhotosService.addPreviewPictureIfExist(products));

    }
}

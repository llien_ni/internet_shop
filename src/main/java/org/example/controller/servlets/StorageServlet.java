package org.example.controller.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.dto.FileDto;
import org.example.model.repositories.FileInfoRepository;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.FilesService;
import org.example.model.services.impl.FilesServiceImpl;
import org.example.model.services.impl.ProductPhotosServiceImpl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@WebServlet("/storage/*")
public class StorageServlet extends HttpServlet {
    private String STORAGE_PATH;
    private FilesService filesService;

    @Override
    public void init() throws ServletException {
        filesService = new FilesServiceImpl((FileInfoRepository) getServletContext().getAttribute("fileInfoRepository"));

        
        STORAGE_PATH = (String) getServletContext().getAttribute("STORAGE_PATH");


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String picturePath = req.getPathInfo().substring(1);

        FileDto fileDto = filesService.getFile(picturePath);

        InputStream inputStream = new FileInputStream(STORAGE_PATH+"\\"+ fileDto.getStorageFileName());

        byte[] imageBytes = inputStream.readAllBytes();
        resp.setContentType(getServletContext().getMimeType(fileDto.getMimeType()));
        resp.setContentLengthLong(fileDto.getSize());
        resp.getOutputStream().write(imageBytes);
    }
}

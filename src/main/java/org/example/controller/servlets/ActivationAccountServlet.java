package org.example.controller.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.models.Account;
import org.example.model.repositories.AccountsRepository;
import org.example.model.services.AccountService;
import org.example.model.services.impl.AccountServiceImpl;
import org.example.model.utils.GenerateLinkUtils;

import java.io.IOException;


@WebServlet("/account_activate")
public class ActivationAccountServlet extends HttpServlet {


    private static final String SIMPLE_ACCOUNT = "simple_account";
    private AccountService accountService;

    @Override
    public void init() throws ServletException {
        accountService = new AccountServiceImpl((AccountsRepository) getServletContext().getAttribute("accountsRepository"));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("id"));

        Account account = accountService.findById(id);
        //добавляет роль simple_account
        if(GenerateLinkUtils.verifyCheckcode(account, req)){
            accountService.addRole(id, SIMPLE_ACCOUNT);
            Account updateAccount = accountService.findById(id);

            req.getSession().setAttribute("account", updateAccount);

        }

        resp.sendRedirect("/home_page");

        //req.getRequestDispatcher("/WEB-INF/jsp/home_page.jsp").forward(req, resp);


    }
}

package org.example.controller.filters.utils;

import jakarta.servlet.http.HttpServletRequest;

import java.util.List;
import java.util.Set;

public class SecurityUtils {

    public static boolean isSecurityPage(HttpServletRequest req){
        String urlPattern = UrlPatternUtils.getUrlPattern(req);


        //тут получили вообще все роли которые есть в приложении
        Set<String> roles = SecureConfig.getAllAppRoles();

        for(String role: roles){

            //получили какие urlPetterns(страницы) доступны для этой роли
            List<String> urlPatterns = SecureConfig.getUrlPatternsForRole(role);

            //если есть страницы для этой роли и страница на которую
            // хотим попасть есть в списке этих страниц, то ретурнуем true
            if(urlPatterns != null && urlPatterns.contains(urlPattern)){
                return  true;
            }

        }
        return false;

    }

    //проверяет подходит роль для этого запроса
    public  static boolean hasPermission(HttpServletRequest req, String role){

        //страница на которую хотим попасть то есть ее юрл
        String urlPattern = UrlPatternUtils.getUrlPattern(req);

        //все роли приложения
        Set<String> allRoles = SecureConfig.getAllAppRoles();

        //страницы доступные для этой роли
        List<String> urlPatterns = SecureConfig.getUrlPatternsForRole(role);
        if(urlPatterns != null && urlPatterns.contains(urlPattern)){
            return true;
        }
        return false;

        //?????? шо це таке
//        for(String role: allRoles){
//            //req.isUserInRole этот метод как то
//            // определяет есть ли роль у пользователя пока что хз как
//            //видимо если пользователь не имеет роди то пропускаем
//
//
//            //есть ли в запросе роль хоть какая то из двух и
//            // дальше если страница на которую
//            // хотим попасть доступна для этой роль то ретурнем true
//            //если страница будет не для этой роли то ретурнется false
//            //или же в реквесте вообще не будет передаваться роли то тоже ретурнется false
//            if(!req.isUserInRole(role)){
//                continue;
//            }
//            //страницы доступные для этой роли
//            List<String> urlPatterns = SecureConfig.getUrlPatternsForRole(role);
//            //страницы для этой роли содержат страницу на которую хотим попасть
//            if(urlPatterns != null && urlPatterns.contains(urlPattern)){
//                return true;
//            }
//        }
//        return false;
    }
}

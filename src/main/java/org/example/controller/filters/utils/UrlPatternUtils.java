package org.example.controller.filters.utils;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletRegistration;
import jakarta.servlet.http.HttpServletRequest;

import java.util.Collection;
import java.util.Map;

public class UrlPatternUtils {

    //есть ли в приложении сервлеты для этих urlPattern'ов
    private static boolean hasUrlPattern(ServletContext context, String urlPattern){
        //получили мапу со всеми сервлетами. ключ это название сервлета
        Map<String, ? extends ServletRegistration> servletRegistrations = context.getServletRegistrations();

        for(String servletName: servletRegistrations.keySet()){

            ServletRegistration servlet = servletRegistrations.get(servletName);

            //это текущие настройки для данного сервлета
            //там указываются urlPattern'ы для этого сервлета
            Collection<String> mappings = servlet.getMappings();

            if(mappings.contains(urlPattern)){
                return true;
            }

        }
        return false;

    }
     public static String getUrlPattern(HttpServletRequest req){
        ServletContext context = req.getServletContext();
        String servletPath = req.getServletPath();

        String pathInfo = req.getPathInfo();

        //тут мы собираем urlPattern, если мы передаем
         // какие то параметры через get метод то
         // они будут в адрессной строке (они находятся в pathInfo)
         // тогда мы заменяем
         // эти параметры на /* чтобы этот запрос попал в фильтр
         //хз может не для этого, но другой причины я не вижу

         String urlPattern = null;
         if(pathInfo !=null){
             urlPattern = servletPath +"/*";
             return urlPattern;
         }
         urlPattern = servletPath;

         //есть ли у нас сервлет для этого url pattern'а
         boolean has = hasUrlPattern(context, urlPattern);
          if(has){
              return urlPattern;
          }

          return "/";


     }


}

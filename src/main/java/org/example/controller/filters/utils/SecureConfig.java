package org.example.controller.filters.utils;

import java.util.*;

public class SecureConfig {

    public static final String ROLE_SIMPLE_ACC = "simple_account";
    public static final String ROLE_PROF_ACC = "professional_account";

    //это вроде мапа соответсвтвия роли и страниц которые доступны этой роли
    private static final Map<String, List<String>> mapConfig = new HashMap<String, List<String>>();

    static{
        init();
    }
    public static void init(){
        //конфигурация для роли simple acc
        List<String> urlPatterns1 = new ArrayList<>();
        //страницы на которые может зайти простой пользователь
        urlPatterns1.add("/account");
        urlPatterns1.add("/order");
        urlPatterns1.add("/account/chats");
        urlPatterns1.add("/account/chat");
        urlPatterns1.add("/account/upload");

        mapConfig.put(ROLE_SIMPLE_ACC, urlPatterns1);

        List<String> urlPatterns2 = new ArrayList<>();

        urlPatterns2.add("/account/add_product");
        urlPatterns2.add("/account/delete_product");
        urlPatterns2.add("/account/products");
        urlPatterns2.add("/account/update_product");
        urlPatterns2.add("/order");
        urlPatterns2.add("/account");
        urlPatterns2.add("/account/chats");
        urlPatterns2.add("/account/chat");
        urlPatterns2.add("/account/upload");

        mapConfig.put(ROLE_PROF_ACC, urlPatterns2);


    }
    public static Set<String> getAllAppRoles(){
        return mapConfig.keySet();
    }
    public static List<String> getUrlPatternsForRole(String role){
        return mapConfig.get(role);
    }



}

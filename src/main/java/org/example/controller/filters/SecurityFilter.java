package org.example.controller.filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.model.models.Account;
import org.example.controller.filters.utils.SecurityUtils;

import java.io.IOException;



//это фильтр вообще на все запросы
@WebFilter("/*")
public class SecurityFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;


        //возвращает путь до сервлета без переданных параметров
        String servletPath = req.getServletPath();

        Account account = (Account) req.getSession().getAttribute("account");

        //если мы и так хотим попасть в логин то он перенаправляет в логин
        if(servletPath.equals("/login")){
            chain.doFilter(req, resp);
            return;
        }
        if(servletPath.equals("/reg")){
            chain.doFilter(req, resp);
            return;
        }

       // HttpServletRequest wrapRequest = req;

        //тут не дописано пока хз че делать
        String role = null;
        if(account != null){
            role = account.getRole();
            System.out.println(role);
        }

        //если это страница не для обычных
        // челиков и для нее нужен вход
        if(SecurityUtils.isSecurityPage(req)){
            //если челик еще не залогинелся, то
            // перенаправляем на залогинивание
            if(account == null){


                resp.sendRedirect("/login");


                return;
            }


            //прошли предыдущий if значит челик залогинен и значит у него есть какая то роль
            // теперь проверяем роль этого челика
            boolean hasPermission = SecurityUtils.hasPermission(req, role);
            if(!hasPermission){
                System.out.println("нет доступа");
                req.getRequestDispatcher("/WEB-INF/jsp/oops.jsp").forward(req, resp);
                return;
            }
        }
        chain.doFilter(req, resp);

    }
}

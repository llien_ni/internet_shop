package org.example.model.connection;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.example.model.jdbc.SimpleDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

public class ConnectionToDataBase {
    private  static final Properties properties = new Properties();



    public static DataSource connection(){

        try {
            properties.load(ConnectionToDataBase.class.getResourceAsStream("/db.properties"));

        } catch (
                IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password"));

        return dataSource;
//        HikariConfig config = new HikariConfig();
//        config.setUsername(properties.getProperty("db.username"));
//        config.setPassword(properties.getProperty("db.password"));
//        config.setDriverClassName(properties.getProperty("db.driver-class-name"));
//        config.setJdbcUrl(properties.getProperty("db.url"));
//        config.setMaximumPoolSize(Integer.parseInt(properties.getProperty("db.hikari.max-pool-size")));
//
//        HikariDataSource dataSource = new HikariDataSource(config);
//
//
//        return dataSource;


    }
}

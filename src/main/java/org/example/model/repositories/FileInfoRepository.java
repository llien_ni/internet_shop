package org.example.model.repositories;

import org.example.model.models.FileInfo;

import java.util.Optional;

public interface FileInfoRepository {

    Long save(FileInfo fileInfo);

    Optional<FileInfo> findByStorageName (String storageFileName);
    Optional<FileInfo> findById (Long id);

    void delete(Long id);



}

package org.example.model.repositories;

import org.example.model.models.Account;

import java.util.Optional;

public interface AccountsRepository {

    void addRole(Long id, String role);


    void addFileToAccount(Long accountId, Long fileId);
    String getFileForAccount(Long accountId);
    void deleteFile(Long accountId);
    Long getIdFileForAccount(Long accountId);


    Long save(Account account);

    void delete(Long id);

    void update(Long id, Account account);

    Optional<Account> findById(Long id);

    boolean emailIsExist(String email);

    Optional<Account> findByEmailAndPassword(String email, byte[] passwordHash);


    String getRoleByEmailPassword(String email, byte[] passwordHash);



    byte[] getSaltForEmail(String email);

    byte[] getPasswordHash(String email);
    byte[] getPasswordHash(Long id);

}

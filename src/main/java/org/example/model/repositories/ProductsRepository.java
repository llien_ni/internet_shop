package org.example.model.repositories;

import org.example.model.models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductsRepository {

    Long save(Product product);

    Optional<Product> findById(Long id);

    List<Product> findByName(String name);

    List<Product> findAll();

    List<Product> findWhereType(String type);

    List<Product> findProductsAddedThisAccount(Long id);

    List<Product> findProductsBoughtAccount(Long id);

    void deleteById(Long id);



    void update(Product product, Long id);


    void addProductToOrder(Long productId, Long accountId);


    void addFileToProduct(Long productId, Long fileId);
    void deleteFileForProduct(Long productId, Long fileId);
    void deleteAllFilesForProduct(Long productId);

    List<String> getFilesForProduct(Long id);


}

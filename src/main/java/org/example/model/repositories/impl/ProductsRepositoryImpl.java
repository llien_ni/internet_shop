package org.example.model.repositories.impl;

import org.example.model.models.Product;
import org.example.model.repositories.ProductsRepository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class ProductsRepositoryImpl implements ProductsRepository {

    private final DataSource dataSource;

    public ProductsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    private static final Function<ResultSet, Product> productMapper = row -> {
        try {
            return Product.builder()
                    .id(row.getLong("id"))
                    .name(row.getString("name"))
                    .description(row.getString("description"))
                    .accountId(row.getLong("account_id"))
                    .productType(row.getString("product_type"))
                    .count(row.getInt("count"))
                    .date(row.getDate("date"))
//                    .photo(row.getString("photo"))
                    .price(row.getInt("price"))
                    .build();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    };

    //save
    //SQL
    private static final String SQL_INSERT_PRODUCT = "insert into product(name, description, account_id, product_type, date, price, count) values (?, ?, ?, ?, ?, ?, ?); ";

    @Override
    public Long save(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PRODUCT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, product.getName());
            statement.setString(2, product.getDescription());
            statement.setLong(3, product.getAccountId());
            statement.setString(4, product.getProductType());
            statement.setDate(5, product.getDate());
            statement.setInt(6, product.getPrice());
            statement.setInt(7, product.getCount());

            int affectRows = statement.executeUpdate();
            if (affectRows != 1) {
                throw new SQLException("Can't save product");
            }
            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                product.setId(generatedKeys.getLong("id"));
                return product.getId();
            } else {
                throw new SQLException("Can't generate product id");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }
    //find
    //SQL
    private static final String SQL_SELECT_BY_ID = "select * from product where id =?;";

    @Override
    public Optional<Product> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {

            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Product product = productMapper.apply(resultSet);
                    return Optional.of(product);
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }

    //find
    //SQL
    private static final String SQL_SELECT_BY_NAME = "select * from product where name like ?;";

    @Override
    public List<Product> findByName(String name) {

        List<Product> products = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_NAME)) {
            statement.setString(1, "%"+name+"%") ;

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Product product = productMapper.apply(resultSet);
                    products.add(product);
                }
            }
            return products;


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }

    //findAll
    //SQL
    private static final String SQL_SELECT_ALL_PRODUCTS = "select * from product;";

    @Override
    public List<Product> findAll() {
        List<Product> products = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement statementSelectProjects = connection.prepareStatement(SQL_SELECT_ALL_PRODUCTS);
            try (ResultSet resultSet = statementSelectProjects.executeQuery()) {
                while (resultSet.next()) {
                    Product product = productMapper.apply(resultSet);



                    products.add(product);
                }
            }

            return products;


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }


    //language=SQL
    private static final String SQL_SELECT_PRODUCTS_WITH_TYPE = "select * from product where product_type = ?;";


    @Override
    public List<Product> findWhereType(String type) {
        List<Product> products = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_PRODUCTS_WITH_TYPE);
            statement.setString(1, type);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Product product = productMapper.apply(resultSet);

                    products.add(product);
                }
            }

            return products;


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    //language=SQL
    private static final String SQL_SELECT_PRODUCTS_ADDED_THIS_ACCOUNT = "select * from product where account_id = ?;";


    @Override
    public List<Product> findProductsAddedThisAccount(Long accountId) {
        List<Product> products = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_PRODUCTS_ADDED_THIS_ACCOUNT);
            statement.setLong(1, accountId);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Product product = productMapper.apply(resultSet);

                    products.add(product);
                }
            }

            return products;


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    //language=SQL
    private static final String SQL_SELECT_PRODUCTS_BOUGHT_THIS_ACCOUNT = "select * from account_product where account_id = ?;";

    @Override
    public List<Product> findProductsBoughtAccount(Long id) {
        List<Product> products = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_PRODUCTS_BOUGHT_THIS_ACCOUNT);) {


            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {

                    Long productId = resultSet.getLong("product_id");
                    Optional<Product> product = findById(productId);
                    product.ifPresent(products::add);




                }
            }

            return products;


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    //language=SQL
    private static final String SQL_DELETE_PRODUCT = "delete from account_product where product_id = ?;delete from product where id = ?;";


    @Override
    public void deleteById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_PRODUCT)) {


            statement.setLong(1, id);
            statement.setLong(2, id);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }


    //SQL
    private static final String SQL_UPDATE_PRODUCT = "update product set name = ?, description = ?, product_type = ?, count = ?, price=? where id = ?;";

    @Override
    public void update(Product product, Long id) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PRODUCT)) {

            statement.setString(1, product.getName());
            statement.setString(2, product.getDescription());
            statement.setString(3, product.getProductType());
//            statement.setString(4, product.getPhoto());
            statement.setInt(4, product.getCount());
            statement.setInt(5, product.getPrice());

            statement.setLong(6, id);

            statement.executeUpdate();


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    //language=SQL
    private static final String SQL_ADD_PRODUCT_TO_ORDER = "insert into account_product(account_id, product_id) values (?, ?);";

    @Override
    public void addProductToOrder(Long productId, Long accountId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_PRODUCT_TO_ORDER)) {


            statement.setLong(1, accountId);
            statement.setLong(2, productId);

            int affectRows = statement.executeUpdate();
            if (affectRows != 1) {
                throw new SQLException("Can't add product to order");
            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }


    //language=SQL
    private static final String SQL_SELECT_FILES = "select product_id, storage_file_name from product_file_photo " +
            "left join " +
            "file_info fi on fi.id = product_file_photo.file_id " +
            "where product_id = ?;";

    @Override
    public List<String> getFilesForProduct(Long id) {



        List<String> filesPaths = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_FILES)) {


            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {


                    filesPaths.add(resultSet.getString("storage_file_name"));
                }
            }

            return filesPaths;


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    //language=SQL
    private static final String SQL_INSERT_PHOTO_TO_PRODUCT = "insert into product_file_photo(product_id, file_id) values(?, ?);";

    @Override
    public void addFileToProduct(Long productId, Long fileId) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PHOTO_TO_PRODUCT)){

            statement.setLong(1, productId);
            statement.setLong(2, fileId);

            statement.executeUpdate();



        }catch (SQLException e){
            throw new IllegalArgumentException("Can't add photo");
        }

    }
    //language=SQL
    private static final String SQL_DELETE_PHOTO_FROM_PRODUCT = "delete  from product_file_photo where product_id =? and file_id=?;";


    @Override
    public void deleteFileForProduct(Long productId, Long fileId) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_PHOTO_FROM_PRODUCT)){

            statement.setLong(1, productId);
            statement.setLong(2, fileId);

            statement.executeUpdate();



        }catch (SQLException e){
            throw new IllegalArgumentException("Can't delete photo");
        }
    }

    //language=SQL
    private static final String SQL_DELETE_ALL_PHOTO_FROM_PRODUCT = "delete  from product_file_photo where product_id =?;";


    @Override
    public void deleteAllFilesForProduct(Long productId) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_ALL_PHOTO_FROM_PRODUCT)){

            statement.setLong(1, productId);


            statement.executeUpdate();



        }catch (SQLException e){
            throw new IllegalArgumentException("Can't delete photo");
        }
    }


}


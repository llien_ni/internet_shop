package org.example.model.repositories.impl;

import org.example.model.models.Account;
import org.example.model.models.Product;
import org.example.model.repositories.AccountsRepository;
import org.example.model.utils.PasswordSecurity;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

public class AccountsRepositoryImpl implements AccountsRepository {

    private final DataSource dataSource;

    public AccountsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, Account> accountMapper = row -> {


        try {
            return Account.builder()
                    .id(row.getLong("id"))
                    .email(row.getString("email"))
                    .firstName(row.getString("first_name"))
                    .lastName(row.getString("last_name"))
                    .age(row.getInt("age"))
                    .photo(row.getString("file_id"))
                    .role(row.getString("role"))
                    .randomCode(row.getString("random_code_for_activation"))
                    .build();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;


    };
    //language=SQL
    private static final String SQL_UPDATE_ROLE = "update account set role=?::status where id = ?;";


    @Override
    public void addRole(Long id, String role) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_ROLE)) {


            statement.setString(1, role);
            statement.setLong(2, id);

            statement.executeUpdate();


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public String getRoleByEmailPassword(String email, byte[] passwordHash) {

        return findByEmailAndPassword(email, passwordHash).get().getRole();
    }

    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL_AND_PASSWORD = "select * from account where email = ? and password_hash = ?;";


    @Override
    public Optional<Account> findByEmailAndPassword(String email, byte[] passwordHash) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_EMAIL_AND_PASSWORD)) {

            statement.setString(1, email);
            statement.setBytes(2, passwordHash);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {

                    Account account = accountMapper.apply(resultSet);
                    return Optional.of(account);
                } else {
                    return Optional.empty();
                }
            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }


    private String generateRandomCode() {
        return UUID.randomUUID().toString();
    }

    //language=SQL
    private static final String SQL_INSERT_ACCOUNT = "insert into account(email, salt, password_hash, first_name, last_name, age, random_code_for_activation) values (?, ?, ?, ?, ?, ?, ?);";

    @Override
    public Long save(Account account) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_ACCOUNT, Statement.RETURN_GENERATED_KEYS)) {


            byte[] salt = PasswordSecurity.generatedSalt();
            byte[] password_hash = PasswordSecurity.calculateHash(account.getPassword(), salt);


            statement.setString(1, account.getEmail());
            statement.setBytes(2, salt);
            statement.setBytes(3, password_hash);
            statement.setString(4, account.getFirstName());
            statement.setString(5, account.getLastName());
            statement.setInt(6, account.getAge());
            statement.setString(7, generateRandomCode());


            int affectRows = statement.executeUpdate();

            if (affectRows != 1) {
                throw new SQLException("I'm so sorry, but I can't save user.");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();


            if (generatedKeys.next()) {
                account.setId(generatedKeys.getLong("id"));

                return account.getId();
            } else {
                throw new SQLException("I can't generated id");
            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }


    private static final String SQL_DELETE_ACCOUNT = "delete  from account_product where account_id=?; delete from product where account_id = ?;delete from account where id = ?;";

    @Override
    public void delete(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_ACCOUNT)) {


            statement.setLong(1, id);
            statement.setLong(2, id);
            statement.setLong(3, id);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }


    //SQL
    private static final String SQL_UPDATE_ACCOUNT = "update account set first_name=?, last_name=?, age=? where id = ?;";

    @Override
    public void update(Long id, Account account) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_ACCOUNT)) {

            statement.setString(1, account.getFirstName());
            statement.setString(2, account.getLastName());
            statement.setInt(3, account.getAge());

            statement.setLong(4, id);

            statement.executeUpdate();


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from account where id = ?;";

    @Override
    public Optional<Account> findById(Long id) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Account account = accountMapper.apply(resultSet);
                    return Optional.of(account);
                } else {
                    return Optional.empty();
                }
            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }

    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL = "select id from account where email = ?;";

    @Override
    public boolean emailIsExist(String email) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_EMAIL)) {


            statement.setString(1, email);

            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }



    }


    //language=SQL
    private static final String SQL_SELECT_SALT_FOR_EMAIL = "select salt from account where email =?;";


    @Override
    public byte[] getSaltForEmail(String email) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_SALT_FOR_EMAIL)) {


            statement.setString(1, email);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {

                System.out.println(Arrays.toString(resultSet.getBytes("salt")));
                return resultSet.getBytes("salt");
            } else {
                byte[] bytes = new byte[0];
                return bytes;

            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }

    //language=SQL
    private static final String SQL_SELECT_PASSWORD_HASH_FOR_EMAIL = "select password_hash from account where email =?;";


    @Override
    public byte[] getPasswordHash(String email) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_PASSWORD_HASH_FOR_EMAIL)) {


            statement.setString(1, email);


            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {

                //System.out.println(Arrays.toString(resultSet.getBytes("salt")));
                return resultSet.getBytes("password_hash");
            } else {
                byte[] bytes = new byte[0];
                return bytes;

            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }

    //language=SQL
    private static final String SQL_SELECT_PASSWORD_HASH_FOR_ID = "select password_hash from account where id =?;";


    @Override
    public byte[] getPasswordHash(Long id) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_PASSWORD_HASH_FOR_ID)) {


            statement.setLong(1, id);


            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {

                //System.out.println(Arrays.toString(resultSet.getBytes("salt")));
                return resultSet.getBytes("password_hash");
            } else {
                byte[] bytes = new byte[0];
                return bytes;

            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }

    //language=SQL
    private static final String SQL_INSERT_PHOTO = "update account set file_id = ? where id=?;";

    @Override
    public void addFileToAccount(Long accountId, Long fileId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PHOTO)) {
            statement.setLong(1, fileId);
            statement.setLong(2, accountId);


            statement.executeUpdate();


        } catch (SQLException e) {
            throw new IllegalArgumentException("Can't add photo");
        }

    }

    //language=SQL
    private static final String SQL_SELECT_FILE = "select storage_file_name from account " +
            "left join " +
            "file_info fi on fi.id = account.file_id where account.id = ?;";


    @Override
    public String getFileForAccount(Long accountId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_FILE)) {


            statement.setLong(1, accountId);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {


                    return resultSet.getString("storage_file_name");
                }
            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return null;

    }
    //language=SQL
    private static final String SQL_SELECT_ID_FILE = "select file_id from account where id = ?;";



    @Override
    public Long getIdFileForAccount(Long accountId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ID_FILE)) {


            statement.setLong(1, accountId);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {


                    return resultSet.getLong("file_id");
                }
            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }

    //language=SQL
    private static final String SQL_DELETE_PHOTO = "update account set file_id = null where id=?;";

    @Override
    public void deleteFile(Long accountId) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_PHOTO)) {

            statement.setLong(1, accountId);


            statement.executeUpdate();


        } catch (SQLException e) {
            throw new IllegalArgumentException("Can't delete photo");
        }
    }


}

package org.example.model.repositories.impl;

import org.example.model.models.Chat;
import org.example.model.models.Message;
import org.example.model.repositories.ChatsRepository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ChatsRepositoryImpl implements ChatsRepository {
    private final DataSource dataSource;

    public ChatsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    //language=SQL
    private static final String SQL_SELECT_CHAT = "select id from chat where first_account_id = ? and second_account_id = ? " +
            "union " +
            "select id from chat where first_account_id = ? and second_account_id = ?;";

    @Override
    public Optional<Chat> findChatIdByThisAccountIdAndProducerId(Long thisAccountId, Long otherAccountId) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_CHAT);

            statement.setLong(1, thisAccountId);
            statement.setLong(2, otherAccountId);
            statement.setLong(3, otherAccountId);
            statement.setLong(4, thisAccountId);


            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {

                    Chat chat = Chat.builder()
                            .chatId(resultSet.getLong("id"))
                            .build();


                    return Optional.of(chat);
                } else {
                    return Optional.empty();
                }
            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    //language=SQL
    private static final String SQL_SELECT_ALL_MESSAGES_IN_CHAT = "select * from message where chat_id =?;";

    @Override
    public List<Message> getMessages(Long chatId) {
        List<Message> messages = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_MESSAGES_IN_CHAT)) {

            statement.setLong(1, chatId);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {

                    Message message = Message.builder()
                            .chatId(resultSet.getLong("chat_id"))
                            .text(resultSet.getString("text"))
                            .accountIdSent(resultSet.getLong("account_id"))
                            .time(resultSet.getTime("time"))

                            .build();

                    messages.add(message);

                }

            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return messages;

    }

    //language=SQL
    private static final String SQL_SELECT_CHAT_THIS_USER = "select * from chat where first_account_id = ? or second_account_id = ?;";

    @Override
    public List<Chat> findThisUserChats(Long id) {
        List<Chat> chatsThisUser = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_CHAT_THIS_USER)) {

            statement.setLong(1, id);
            statement.setLong(2, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {

                    Chat chat = Chat.builder()
                            .chatId(resultSet.getLong("id"))
                            .firstAccId(resultSet.getLong("first_account_id"))
                            .secondAccId(resultSet.getLong("second_account_id"))
                            .build();

                    chatsThisUser.add(chat);

                }

            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        return chatsThisUser;
    }

    //language=SQL
    private static final String SQL_INSERT_MESSAGE_TO_CHAT = "insert into message (account_id, chat_id, text, time) values(?, ?, ?, localtimestamp);";

    @Override
    public void addMessageToChat(Long accountId, Long chatId, String message) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_MESSAGE_TO_CHAT)) {

            statement.setLong(1, accountId);
            statement.setLong(2, chatId);
            statement.setString(3, message);
            System.out.println("***");


            int affectRows = statement.executeUpdate();


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    //language=SQL
    private static final String SQL_INSERT_CHAT = "insert into chat (first_account_id, second_account_id) values (?, ?);";

//    //language=SQL
//    private static final String SQL_INSERT_CHAT_TO_USER = "insert into user_chat(user_id, chat_id)" +
//            "values (?, ?);";

    @Override
    public boolean isChatForThisAccountsAlreadyExist(Long thisAccountId, Long otherAccountId) {
        Optional<Chat> chat = findChatIdByThisAccountIdAndProducerId(thisAccountId, otherAccountId);

        return chat.isPresent();
    }


    @Override
    public void createChat(Long thisAccountId, Long otherAccountId) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_CHAT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setLong(1, thisAccountId);
            statement.setLong(2, otherAccountId);


            int affectRows = statement.executeUpdate();

            if (affectRows != 1) {
                throw new SQLException("I'm so sorry, but I can't save chat.");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();


//            if (generatedKeys.next()) {
//                chat.setId(generatedKeys.getLong("id"));
//                System.out.println(chat.getId());
//            } else {
//                throw new SQLException("I can't generated id");
//            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }


    //language=SQL
    private static final String SQL_SELECT_THIS_ACC_MESSAGES_IN_CHAT = "select text from message where chat_id =? and account_id=?;";

    public List<Message> getMessagesSentThisAccountInChat(Long chatId, Long accountId) {


        List<Message> messages = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_THIS_ACC_MESSAGES_IN_CHAT)) {

            statement.setLong(1, chatId);
            statement.setLong(1, accountId);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {

                    Message message = Message.builder()
                            .text(resultSet.getString("text"))
                            .build();

                    messages.add(message);

                }

            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return messages;


    }


}

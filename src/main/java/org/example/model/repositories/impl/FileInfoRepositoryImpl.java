package org.example.model.repositories.impl;

import org.example.model.models.FileInfo;
import org.example.model.repositories.FileInfoRepository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;
import java.util.function.Function;

public class FileInfoRepositoryImpl implements FileInfoRepository {

    private final DataSource dataSource;

    public FileInfoRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, FileInfo> fileInfoMapper = row -> {


        try {
            return FileInfo.builder()
                    .id(row.getLong("id"))
                    .storageFileName(row.getString("storage_file_name"))
                    .originalFileName(row.getString("original_file_name"))
                    .mimeType(row.getString("mime_type"))
                    .size(row.getLong("size"))
                    .build();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;


    };

    //language=SQL
    private static final String SQL_INSERT = "insert into file_info(original_file_name, storage_file_name, size, mime_type) " +
            "values (?, ?, ?, ?);";

    @Override
    public Long save(FileInfo fileInfo) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {


            statement.setString(1, fileInfo.getOriginalFileName());

            statement.setString(2, fileInfo.getStorageFileName());
            statement.setLong(3, fileInfo.getSize());
            statement.setString(4, fileInfo.getMimeType());


            int affectRows = statement.executeUpdate();

            if (affectRows != 1) {
                throw new SQLException("I'm so sorry, but I can't save file.");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();


            if (generatedKeys.next()) {
                fileInfo.setId(generatedKeys.getLong("id"));

                return fileInfo.getId();

            } else {
                throw new SQLException("I can't generated id");
            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }


    //language=sql
    private static final String SQL_SELECT_BY_STORAGE_NAME = "select * from file_info where storage_file_name=?;";


    @Override
    public Optional<FileInfo> findByStorageName(String storageFileName) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_STORAGE_NAME)) {

            statement.setString(1, storageFileName);

            try (ResultSet resultSet = statement.executeQuery()) {
                if(resultSet.next()){
                    return Optional.of(fileInfoMapper.apply(resultSet));
                }
                return Optional.empty();

            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
    //language=sql
    private static final String SQL_SELECT_BY_ID = "select * from file_info where id=?;";


    @Override
    public Optional<FileInfo> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {

            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                if(resultSet.next()){
                    return Optional.of(fileInfoMapper.apply(resultSet));
                }
                return Optional.empty();

            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    //language=SQL
    private static final String SQL_DELETE_FILE = "delete from file_info where id = ?;";

    @Override
    public void delete(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_FILE)) {


            statement.setLong(1, id);


            statement.executeUpdate();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }
}

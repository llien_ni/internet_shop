package org.example.model.repositories;

import org.example.model.models.Chat;
import org.example.model.models.Message;

import java.util.List;
import java.util.Optional;

public interface ChatsRepository {


    Optional<Chat> findChatIdByThisAccountIdAndProducerId(Long thisAccountId, Long otherAccountId);



    boolean isChatForThisAccountsAlreadyExist(Long thisAccountId, Long otherAccountId);


    void addMessageToChat(Long accountId, Long chatId, String message);

    //создаем чат если у них нет чата
    void createChat(Long thisAccountId, Long otherAccountId);


    //находит все сообщения
    List<Message> getMessages(Long chatId);

    List<Chat> findThisUserChats(Long id);


    List<Message> getMessagesSentThisAccountInChat(Long chatId, Long accountId);
}

package org.example.model.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Account {
    private Long id;
    private String firstName;
    private String lastName;
    private Integer age;
    private String email;//должен быть уникальным для каждого челика
    private String password;
    private String photo;

    //случайный код для активации аккаунта
    private String randomCode;

    private String role;

}

package org.example.model.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class Message {

    private Long chatId;
    private Long accountIdSent;
    private String text;
    private Time time;
}

package org.example.model.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.sql.SQLType;


@AllArgsConstructor
@NoArgsConstructor
@lombok.Data
@Builder
public class Product {

    private Long id;
    private Integer count;
    private String name;
    private String description;
    private Long accountId;//это id аккаунта, который продает товар, то есть он его добавил

    private String productType;

    private Date date;
    private String photo;
    private Integer price;
}

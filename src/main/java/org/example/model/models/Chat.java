package org.example.model.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Chat {

    private Long chatId;

    private Long firstAccId;
    private Long secondAccId;

    private List<Message> messages;
}

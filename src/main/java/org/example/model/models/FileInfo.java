package org.example.model.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FileInfo {



    private Long id;
    private String originalFileName;

    private String storageFileName;

    private Long size;

    //тип файла
    private String mimeType;
}

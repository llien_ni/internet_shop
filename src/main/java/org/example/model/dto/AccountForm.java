package org.example.model.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class AccountForm {
    private String firstName;
    private String lastName;
    private Integer age;
    private String email;
    private String password;


}

package org.example.model.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.model.models.Account;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class AccountDto {
    private String firstName;
    private String lastName;
    private Integer age;
    private String email;
    private String photo;
    private String role;
    private Long id;

    public static AccountDto from(Account account){
        return AccountDto.builder()
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .email(account.getEmail())
                .age(account.getAge())
                .id(account.getId())
                .role(account.getRole())
                .build();
    }
    public static AccountDto from(Account account, String photo){
        return AccountDto.builder()
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .email(account.getEmail())
                .age(account.getAge())
                .photo(photo)
                .role(account.getRole())
                .id(account.getId())
                .build();
    }



}

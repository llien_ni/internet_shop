package org.example.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ProductAddForm {


    private String name;
    private String description;
    private Integer count;

    private String productType;


    private Integer price;
}

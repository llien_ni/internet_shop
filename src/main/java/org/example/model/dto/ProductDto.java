package org.example.model.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.model.models.Product;

import java.io.File;
import java.nio.file.Files;
import java.sql.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ProductDto {

    private Long id;
    private Integer count;
    private String name;
    private String description;
    private List<String> allPhotos;


    private Long accountId;

    private String productType;

    private Date date;

    private String photo;
    private Integer price;

    public static ProductDto from(Product product){

        return ProductDto.builder()
                .count(product.getCount())
                .date(product.getDate())
                .description(product.getDescription())
                .productType(product.getProductType())
                .name(product.getName())
                .id(product.getId())
                .accountId(product.getAccountId())
                .price(product.getPrice())
                .build();

    }
    public static ProductDto from(Product product, List<String> files){

        return ProductDto.builder()
                .count(product.getCount())
                .date(product.getDate())
                .description(product.getDescription())
                .productType(product.getProductType())
                .name(product.getName())
                .id(product.getId())
                .price(product.getPrice())
                .accountId(product.getAccountId())
                .allPhotos(files)
                .build();

    }
    public static ProductDto from(Product product, String file){

        return ProductDto.builder()
                .count(product.getCount())
                .date(product.getDate())
                .description(product.getDescription())
                .productType(product.getProductType())
                .name(product.getName())
                .id(product.getId())
                .price(product.getPrice())
                .accountId(product.getAccountId())
                .photo(file)
                .build();

    }






}

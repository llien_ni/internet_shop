package org.example.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.model.models.FileInfo;
import org.example.model.models.Message;

import java.sql.Time;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MessageDto {
    private Long chatId;
    private String accountFirstName;
    private String text;

    private Time time;

    public static MessageDto from(Message message, String name){

        return MessageDto.builder()
                .accountFirstName(name)
                .text(message.getText())
                .time(message.getTime())
                .build();
    }
}

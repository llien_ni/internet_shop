package org.example.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.model.models.FileInfo;

import java.io.InputStream;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FileDto {



    private Long id;
    private Long size;
    private String storageFileName;
    private String mimeType;


    private InputStream fileStream;

    private String originalFileName;
    public static FileDto from(FileInfo fileInfo){

        return FileDto.builder()
                .id(fileInfo.getId())
                .storageFileName(fileInfo.getStorageFileName())
                .size(fileInfo.getSize())
                .mimeType(fileInfo.getMimeType())
                .originalFileName(fileInfo.getOriginalFileName())
                .build();
    }
}

package org.example.model.utils;

import jakarta.mail.*;
import jakarta.mail.Authenticator.*;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import org.example.model.models.Account;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

public class EmailUtils {
    private static final String FROM = "examplefor575@gmail.com";

    public static void sendAccountActivateEmail(Account account){

        Session session = getSession();


        //это класс для отправки email сообщений MIME
        //вот это видимо создает пустое сообщение
        MimeMessage message = new MimeMessage(session);
        try{

            message.setSubject("Почта активации аккаунта");
            //new Date() сразу возвращает текущее время
            message.setSentDate(new Date());
            //делает из строки интернет адрес
            message.setFrom(new InternetAddress(FROM));
            //recipient-получатель
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(account.getEmail()));
            message.setContent("<a href='"+ GenerateLinkUtils.generateActivateLink(account)+"'> click this to activate account</a>","text/html;charset=utf-8");


            //отправляет
            Transport.send(message);


        }catch (Exception e){
            throw new IllegalArgumentException(e);
        }

    }



    //возвращает настройки по которым создается сессия для отправки письма
    public static Session getSession(){

        Properties properties = new Properties();

        //это протокол smtp
        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.transport.protocol", "smtp");


        //smtp-сервер для подключения
        properties.setProperty("mail.smtp.host", "smtp.gmail.com");


        properties.setProperty("mail.smtp.port", "587");


        properties.setProperty("mail.smtp.auth", "true");

        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                String password = null;
                InputStream is = EmailUtils.class.getResourceAsStream("/password.dat");


                byte[] b = new byte[1024];
                try{
                    int len = is.read(b);
                    password = new String(b, 0, len);

                }catch (IOException e){
                    e.printStackTrace();
                }
                return new PasswordAuthentication(FROM, password);


            }
        });
        return session;
    }

}

package org.example.model.utils;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import org.example.model.models.Account;

public class GenerateLinkUtils {

    private static final String CHECK_CODE = "checkCode";

    public static String generateActivateLink(Account account){
        return "http://localhost:8080/account_activate?id="+account.getId()+"&"+CHECK_CODE+"="+generateCheckCode(account);


    }
    //это получает код который есть у аккаунта который мы до этого сохранили
    public static String generateCheckCode(Account account){

        //это поле рандомного кода которое есть у аккаунта
        String randomCode = account.getRandomCode();


        return randomCode;

    }

    public static boolean verifyCheckcode(Account account, HttpServletRequest req){
        String checkCode = req.getParameter(CHECK_CODE);
        return generateCheckCode(account).equals(checkCode);
    }
}

package org.example.model.services;

import org.example.model.dto.ProductDto;
import org.example.model.models.Product;

import java.util.List;

public interface ProductPhotosService {

    void addFileToProduct(Long productId, Long fileId);


    List<String> getFilesForProduct(Long productId);


    List<ProductDto> addPreviewPictureIfExist(List<Product> products);
    ProductDto addPicturesIfExists(Product product);

    void deleteFileForProduct(Long productId, Long fileId);
    void deleteAllFilesForProduct(Long productId);


}

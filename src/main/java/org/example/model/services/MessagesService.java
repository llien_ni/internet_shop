package org.example.model.services;

import org.example.model.dto.MessageDto;
import org.example.model.models.Message;

import java.util.List;

public interface MessagesService {

    MessageDto addNameWhichSentMessage(Message message);

    List<MessageDto> convertMessages(List<Message> messages);
}

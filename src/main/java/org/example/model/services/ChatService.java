package org.example.model.services;

import org.example.model.models.Chat;
import org.example.model.models.Message;

import java.util.List;
import java.util.Optional;

public interface ChatService {


    Optional<Chat> findChatIdByThisAccountIdAndProducerId(Long thisAccountId, Long otherAccountId);



    boolean isChatForThisAccountsAlreadyExist(Long thisAccountId, Long otherAccountId);

    List<Chat> findThisUserChats(Long id);

    List<Message> getMessages(Long chatId);

    void addMessageToChat(Long accountId, Long chatId, String message);

    //создаем чат если у них нет чата
    void createChat(Long thisAccountId, Long otherAccountId);




}

package org.example.model.services;

import org.example.model.dto.ProductAddForm;
import org.example.model.models.Product;

import java.util.List;

public interface ProductService {

    Long addProduct(ProductAddForm form, Long accountId);
    void update(ProductAddForm form, Long id);
    void delete(Long id);
    Product findById(Long id);
    List<Product> findByName(String name);
    List<Product> findAll();


    List<Product> findProductsAddedThisAccount(Long id);
    List<Product> findProductsBoughtAccount(Long id);
    List<Product> findWhereType (String type);


    void addProductToOrder(Long productId, Long accountId);


}

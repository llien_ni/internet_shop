package org.example.model.services;

import org.example.model.dto.AccountDto;
import org.example.model.dto.FileDto;
import org.example.model.models.Account;

import java.util.List;

public interface AccountPhotosService {
    void addFileToAccount(Long accountId, Long fileId);



    String getFileForAccount(Long accountId);

    Long getIdFileForAccount(Long accountId);


    void deleteFileForAccount(Long accountId);
    AccountDto addPreviewPhotoIfExist(Account account);
}

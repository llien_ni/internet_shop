package org.example.model.services;

import java.util.List;

public interface DeletePhotoService {

    void deletePhotos(Long productId);
    void deletePhoto(Long productId, List<String> photos);

}

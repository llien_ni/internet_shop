package org.example.model.services;

import org.example.model.dto.AccountDto;
import org.example.model.dto.FileDto;
import org.example.model.dto.ProductDto;
import org.example.model.models.Product;

import java.io.File;
import java.io.OutputStream;
import java.util.List;

public interface FilesService {

    Long upload(FileDto form);

    void setStoragePath(String path);

    FileDto getFile(String storageFileName);

    FileDto getFile(Long id);

    void deleteFile(FileDto form);





}

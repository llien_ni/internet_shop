package org.example.model.services.impl;

import org.example.model.dto.FileDto;
import org.example.model.dto.ProductDto;
import org.example.model.models.FileInfo;
import org.example.model.models.Product;
import org.example.model.repositories.FileInfoRepository;
import org.example.model.services.FilesService;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class FilesServiceImpl implements FilesService {
    private final FileInfoRepository fileInfoRepository;

    private String storagePath;

    public FilesServiceImpl(FileInfoRepository fileInfoRepository) {
        this.fileInfoRepository = fileInfoRepository;
    }



    @Override
    public Long upload(FileDto form) {

        String fileName = form.getOriginalFileName();
        String extension = fileName.substring(fileName.lastIndexOf("."));

        FileInfo fileInfo = FileInfo.builder()
                .originalFileName(fileName)
                .storageFileName(UUID.randomUUID()+extension)
                .mimeType(form.getMimeType())
                .size(form.getSize())
                .build();

        //сохраняет в базу
        Long fileId = fileInfoRepository.save(fileInfo);
        //сохраняет в папку
        if(storagePath != null){
            try {
                Files.copy(form.getFileStream(), Paths.get(storagePath +"\\"+fileInfo.getStorageFileName()));

            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }

        }

        return fileId;

    }

    @Override
    public void setStoragePath(String path) {

        this.storagePath = path;

    }

    @Override
    public FileDto getFile(String storageFileName) {
        Optional<FileInfo> fileInfoOptional = fileInfoRepository.findByStorageName(storageFileName);

        if(fileInfoOptional.isPresent()){
            //получаем дтошку из модели
            FileDto file = FileDto.from(fileInfoOptional.get());


            file.setStorageFileName(storageFileName);

            return file;

        }

        return null;
    }

    @Override
    public FileDto getFile(Long id) {
        Optional<FileInfo> fileInfoOptional = fileInfoRepository.findById(id);

        if(fileInfoOptional.isPresent()){
            //получаем дтошку из модели
            FileDto file = FileDto.from(fileInfoOptional.get());


            //file.setFileName(storageFileName);

            return file;

        }

        return null;
    }


    @Override
    public void deleteFile(FileDto form) {
        fileInfoRepository.delete(form.getId());

        Path path = Paths.get(storagePath, form.getStorageFileName());
        System.out.println("heello from delete");
        System.out.println(path);
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

package org.example.model.services.impl;

import org.example.model.dto.FileDto;
import org.example.model.services.DeletePhotoService;
import org.example.model.services.FilesService;
import org.example.model.services.ProductPhotosService;

import java.util.List;

public class DeletePhotoServiceImpl implements DeletePhotoService {
    private final ProductPhotosService productPhotosService;

    private final FilesService filesService;

    public DeletePhotoServiceImpl(ProductPhotosService productPhotosService, FilesService filesService) {
        this.productPhotosService = productPhotosService;
        this.filesService = filesService;
    }

    @Override
    public void deletePhotos(Long productId) {
        List<String> filesForProduct = productPhotosService.getFilesForProduct(productId);

        productPhotosService.deleteAllFilesForProduct(productId);

        //удалили все файлы
        for(String fileStorageName:filesForProduct){
            //FileDto file = filesService.getFile(fileStorageName);

            //это удаляет и из базы и из памяти
            filesService.deleteFile(filesService.getFile(fileStorageName));


        }
    }


    @Override
    public void deletePhoto(Long productId, List<String> photos) {
        for(String photo: photos){

            FileDto file = filesService.getFile(photo);


            //это мы удалили из базы
            productPhotosService.deleteFileForProduct(productId, file.getId());

            filesService.deleteFile(file);

        }
    }
}

package org.example.model.services.impl;

import org.example.model.dto.AccountForm;
import org.example.model.models.Account;
import org.example.model.repositories.AccountsRepository;
import org.example.model.services.AccountService;

import java.util.Optional;

public class AccountServiceImpl implements AccountService {


    private final AccountsRepository accountsRepository;

    public AccountServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public Account findByEmailAndPassword(String email, byte[] passwordHash){
        Optional<Account> account = accountsRepository.findByEmailAndPassword(email, passwordHash);

        if(account.isEmpty()){
            throw new IllegalArgumentException("Email or password is incorrect");
        }
        return account.get();
    }


    @Override
    public Account findById(Long id) {
        Optional<Account> account = accountsRepository.findById(id);
        if(account.isEmpty()){
            throw new IllegalArgumentException("Account isn't exist");
        }else{
            return account.get();
        }
    }

    @Override
    public void delete(Long id) {
        accountsRepository.delete(id);
    }


    @Override
    public boolean isEmailAlreadyExist(String email) throws IllegalArgumentException {

        return accountsRepository.emailIsExist(email);
    }

    @Override
    public void update(Long id, AccountForm form) {
        Account account = Account.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .age(form.getAge())
                .build();
        accountsRepository.update(id, account);

    }

    @Override
    public Long signUp(AccountForm form) {

        Account account = Account.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .age(form.getAge())
                .email(form.getEmail())
                .password(form.getPassword())
                .role("simple_account")
                .build();

        return accountsRepository.save(account);
    }

    @Override
    public void addRole(Long id, String role) {
        accountsRepository.addRole(id, role);
    }

    @Override
    public byte[] getSaltForEmail(String email) {
        return accountsRepository.getSaltForEmail(email);
    }

    @Override
    public byte[] getPasswordHash(String email) {
        return accountsRepository.getPasswordHash(email);
    }

}

package org.example.model.services.impl;

import org.example.model.models.Chat;
import org.example.model.models.Message;
import org.example.model.repositories.ChatsRepository;
import org.example.model.services.ChatService;

import java.util.List;
import java.util.Optional;

public class ChatServiceImpl implements ChatService {

    private final ChatsRepository chatsRepository;

    public ChatServiceImpl(ChatsRepository chatsRepository) {
        this.chatsRepository = chatsRepository;
    }


    @Override
    public Optional<Chat> findChatIdByThisAccountIdAndProducerId(Long thisAccountId, Long otherAccountId) {
        return chatsRepository.findChatIdByThisAccountIdAndProducerId(thisAccountId,otherAccountId);
    }

    @Override
    public boolean isChatForThisAccountsAlreadyExist(Long thisAccountId, Long otherAccountId) {
        return chatsRepository.isChatForThisAccountsAlreadyExist(thisAccountId, otherAccountId);
    }

    @Override
    public void addMessageToChat(Long accountId, Long chatId, String message) {

        chatsRepository.addMessageToChat(accountId, chatId, message);
    }

    @Override
    public List<Message> getMessages(Long chatId) {
        return chatsRepository.getMessages(chatId);
    }

    @Override
    public List<Chat> findThisUserChats(Long id) {
        return chatsRepository.findThisUserChats(id);
    }

    @Override
    public void createChat(Long thisAccountId, Long otherAccountId) {

        chatsRepository.createChat(thisAccountId, otherAccountId);
    }


}

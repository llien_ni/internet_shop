package org.example.model.services.impl;

import org.example.model.dto.MessageDto;
import org.example.model.models.Account;
import org.example.model.models.Message;
import org.example.model.services.AccountService;
import org.example.model.services.MessagesService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MessagesServiceImpl implements MessagesService {

    private final AccountService accountService;

    public MessagesServiceImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public MessageDto addNameWhichSentMessage(Message message) {
        Account account = accountService.findById(message.getAccountIdSent());

        return MessageDto.from(message, account.getFirstName());

    }

    @Override
    public List<MessageDto> convertMessages(List<Message> messages) {

        return messages.stream().map(this::addNameWhichSentMessage).toList();

    }
}

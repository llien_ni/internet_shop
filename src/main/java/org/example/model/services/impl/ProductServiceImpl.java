package org.example.model.services.impl;

import org.example.model.dto.ProductAddForm;
import org.example.model.models.Product;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.ProductService;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class ProductServiceImpl implements ProductService {
    private final ProductsRepository productsRepository;

    public ProductServiceImpl(ProductsRepository productsRepository) {

        this.productsRepository = productsRepository;
    }


    @Override
    public Product findById(Long id) {

        Optional<Product> product = productsRepository.findById(id);
        if(product.isEmpty()){
            return null;
        }else{
            return product.get();
        }


    }

    @Override
    public List<Product> findByName(String name) {
        return productsRepository.findByName(name);
    }

    @Override
    public List<Product> findAll() {
        return productsRepository.findAll();

    }

    @Override
    public List<Product> findProductsAddedThisAccount(Long id) {
        System.out.println("(((");
        System.out.println(productsRepository);
        return productsRepository.findProductsAddedThisAccount(id);
    }

    @Override
    public List<Product> findProductsBoughtAccount(Long id) {
        return productsRepository.findProductsBoughtAccount(id);
    }






    @Override
    public Long addProduct(ProductAddForm form, Long accountId) {

        if (form.getCount() < 1){
            throw new IllegalArgumentException("Count is negative");
        }
        if (form.getPrice() < 1){
            throw new IllegalArgumentException("Price is negative");
        }else {
            Product product = Product.builder()
                    .name(form.getName())
                    .count(form.getCount())
                    .price(form.getPrice())
                    .accountId(accountId)

                    .date(Date.valueOf(LocalDate.now()))
                    .description(form.getDescription())
                    .productType(form.getProductType())
                    .build();

            return productsRepository.save(product);
        }


    }

    @Override
    public void addProductToOrder(Long productId, Long accountId){

        Product product = productsRepository.findById(productId).get();

        if (product.getCount() > 0) {
            productsRepository.addProductToOrder(productId, accountId);
            product.setCount(product.getCount()-1);
            productsRepository.update(product, productId);

        }else{
            throw new IllegalArgumentException("Product count is 0");
        }



    }

    @Override
    public void update(ProductAddForm form, Long id) {
        Product product = Product.builder()
                .name(form.getName())
                .count(form.getCount())
                .price(form.getPrice())
//                .photo(form.getPhoto())
                .description(form.getDescription())
                .productType(form.getProductType())
                .build();

        productsRepository.update(product, id);

    }

    @Override
    public List<Product> findWhereType(String type) {
        return productsRepository.findWhereType(type);
    }

    @Override
    public void delete(Long id) {

        productsRepository.deleteById(id);

    }


}

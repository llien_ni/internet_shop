package org.example.model.services.impl;

import org.example.model.dto.FileDto;
import org.example.model.dto.ProductDto;
import org.example.model.models.Product;
import org.example.model.repositories.ProductsRepository;
import org.example.model.services.FilesService;
import org.example.model.services.ProductPhotosService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProductPhotosServiceImpl implements ProductPhotosService {

    private final ProductsRepository productsRepository;

    public ProductPhotosServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;


    }


    @Override
    public void addFileToProduct(Long productId, Long fileId) {
        productsRepository.addFileToProduct(productId, fileId);
    }



    @Override
    public List<String> getFilesForProduct(Long productId) {
        return productsRepository.getFilesForProduct(productId);
    }



    @Override
    public void deleteFileForProduct(Long productId, Long fileId) {
        productsRepository.deleteFileForProduct(productId, fileId);
    }

    @Override
    public void deleteAllFilesForProduct(Long productId) {

        if(!productsRepository.getFilesForProduct(productId).isEmpty()){
            productsRepository.deleteAllFilesForProduct(productId);
        }


    }
    @Override
    public List<ProductDto> addPreviewPictureIfExist(List<Product> products) {
        List<ProductDto> allProducts = new ArrayList<>();

        for(Product product: products){
            Optional<String> firstFile = getFilesForProduct(product.getId()).stream().findFirst();
            ProductDto productDto = null;
            if(firstFile.isPresent()){

                productDto= ProductDto.from(product, firstFile.get());


            }else{
                productDto  = ProductDto.from(product);

            }
            allProducts.add(productDto);
        }
        return allProducts;
    }

    @Override
    public ProductDto addPicturesIfExists(Product product) {
        List<String> filesForProduct = getFilesForProduct(product.getId());

        ProductDto productDto = null;
        if(filesForProduct != null){
            productDto = ProductDto.from(product, filesForProduct);
        }else{
            productDto = ProductDto.from(product);
        }
        return productDto;
    }


}

package org.example.model.services.impl;

import org.example.model.dto.AccountDto;
import org.example.model.dto.FileDto;
import org.example.model.dto.ProductDto;
import org.example.model.models.Account;
import org.example.model.models.Product;
import org.example.model.repositories.AccountsRepository;
import org.example.model.services.AccountPhotosService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AccountPhotosServiceImpl implements AccountPhotosService {
    private final AccountsRepository accountsRepository;

    public AccountPhotosServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public void addFileToAccount(Long accountId, Long fileId) {

        accountsRepository.addFileToAccount(accountId, fileId);

    }

    @Override
    public String getFileForAccount(Long accountId) {

        return accountsRepository.getFileForAccount(accountId);

    }

    @Override
    public Long getIdFileForAccount(Long accountId) {
        return  accountsRepository.getIdFileForAccount(accountId);

    }

    @Override
    public void deleteFileForAccount(Long accountId) {

        if(accountsRepository.getFileForAccount(accountId)!=null){
            accountsRepository.deleteFile(accountId);
        }

    }

    @Override
    public AccountDto addPreviewPhotoIfExist(Account account) {


        String photoPath = getFileForAccount(account.getId());

        AccountDto accountDto = null;
        if(photoPath != null){


            accountDto = AccountDto.from(account, photoPath);
        }else{
            accountDto = AccountDto.from(account);
        }
        return accountDto;


    }
}

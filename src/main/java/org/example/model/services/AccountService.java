package org.example.model.services;

import org.example.model.dto.AccountForm;
import org.example.model.models.Account;

import java.util.Optional;

public interface AccountService {

    void delete(Long id);
    void update(Long id, AccountForm form);

    Long signUp(AccountForm form);

    Account findById(Long id);

    Account findByEmailAndPassword(String email, byte[] passwordHash) throws IllegalArgumentException;
    boolean isEmailAlreadyExist(String email) throws IllegalArgumentException;


    byte[] getSaltForEmail(String email);


    byte[] getPasswordHash(String email);

    void addRole(Long id, String role);



}

<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 08.10.2022
  Time: 22:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <script defer type="text/javascript" src="../../resources/jquery-3.6.1.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
    <script src="https://malsup.github.io/jquery.form.js"></script>
<%--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"--%>
<%--          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">--%>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>


    <link rel="stylesheet" href="../../resources/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/slick.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/magnific-popup.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/normalize.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/jquery.nice-number.css">
    <link rel="stylesheet" href="../../resources/assets/css/mean-menu.css">
    <link rel="stylesheet" href="../../resources/assets/css/default.css">
    <link rel="stylesheet" href="../../resources/assets/css/style.css">
    <style>
        .button-for-login{

            position: relative;
            left: 0px;
            bottom: 40px;
        }
        form label.error {

            /*padding: px 10px 10px 200px;*/
            color: #f00;
            /* background: #f00; */
            /* width: 140px; */

        }

    </style>


</head>

<body>

<header>
    <div class="header-area header-9 header-resposive pt-30 ">
        <div class="header-top">
            <div class="container">
                <div class="header-top">
                    <div class="row">
                        <div class="col-xl-3 col-lg-4 order-md-1 col-md-5   col-sm-12 col-12   ">
                            <ul class="user-info d-flex pt-15 justify-content-center justify-content-md-start  ">
                                <li><a href="/home_page">Главная страница</a></li>
                                <li><a href="/reg">Зарегистрироваться</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="header-menu mt-15 pt-25 pb-20">
            <div class="container">

                <div class="mobile-menu"></div>
            </div>
        </div>


    </div>
</header>

<!-- =================Header Area Ends================= -->

<!-- =================Page Title Area Starts================= -->

<div class="page-title-area pt-10 pb-10 " style="background-image: url(assets/img/bg/chechout-page-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-titel-detalis  ">
                    <div class="page-title position-relative">
                        <h2>Login Page</h2>
                    </div>
                    <div class="page-bc">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/home_page"> <i
                                        class="fas fa-home "></i>Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a
                                        href="/login">Login Page</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- =================Page Title Area End================= -->
<!-- =================Login Area Starts================= -->

<div class="login-page-area pt-50">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-9">
                <div class="login-detalis pt-40 pr-40 pl-40 pb-40">
                    <form id="form" method="post" action="">
                        <div class="login-input">

                            <div>
                                <input class="required content" type="text" name="email" placeholder="Enter your email....">
                                <label class="messages error"></label>
                            </div>
                            <div>
                                <input class="required password" type="password" name="password"
                                       placeholder="Enter your Password....">
                                <label class="messages error"></label>
                            </div>

                        </div>
                        <label class="messages error">
                            <c:if test="${ !requestScope.error.equals('')}">
                                <c:out value="${requestScope.error}"/>
                            </c:if>
                        </label>


                        <div class="login-button text-center pt-30 pb-10 ">
                            <button class="btn first button-for-login" type="submit" >Login</button>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>


<!-- =================Footer Area Ends================= -->

<!-- scripts -->
<script src="../../resources/assets/js/vendor/jquery.min.js"></script>
<script src="../../resources/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<script src="../../resources/assets/js/vendor/bootstrap.min.js"></script>
<script src="../../resources/assets/js/vendor/popper.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery-mean-menu.min.js"></script>
<script src="../../resources/assets/js/vendor/owl.carousel.min.js"></script>
<script src="../../resources/assets/js/vendor/isotope.pkgd.min.js"></script>
<script src="../../resources/assets/js/countdown.js"></script>
<script src="../../resources/assets/js/vendor/jquery.nice-number.js"></script>
<script src="../../resources/assets/js/vendor/slick.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="../../resources/assets/js/vendor/wow-1.3.0.min.js"></script>
<script src="../../resources/assets/js/main.js"></script>


<script>
    $('document').ready(function () {
        $('#form').validate(
            {
                // правила для проверки
                rules: {
                    content: {
                        required: true,
                        minlength: 2,
                        maxlength: 30
                    },
                    password: {
                        required: true,
                        minlength: 2,
                        maxlength: 30
                    }
                },

                // выводимые сообщения при нарушении соответствующих правил
                messages: {
                    "email": {
                        required: "Это поле не может быть пустым",
                        minlength: "От 2 до 30 символов",
                        maxlength: "От 2 до 30 символов"
                    },
                    "password": {
                        required: "Это поле не может быть пустым",
                        minlength: "От 2 до 30 символов",
                        maxlength: "От 2 до 30 символов"
                    }

                },


                // указаваем обработчик
                submitHandler: function (form) {
                    $(form).ajaxSubmit({
                        target: '#preview',
                        // success: function() {
                        //     $('#contact_form').slideUp("fast", function(){
                        //             $(this).before($("<div id='checkmark'><img src='img/check.png'><p>Ваша заявка принята!</p></div>").delay(6000));
                        //         }
                        //     ).delay(6000).slideDown('fast',function() {$(this).prev().remove();});
                        //     $("#form").clearForm();
                        // }
                    })
                }
            });
    });
</script>

</body>


</html>

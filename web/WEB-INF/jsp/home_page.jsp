<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 08.10.2022
  Time: 22:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="shop, ecommerce, store, multipurpose, shopify, woocommerce, html5, css3, sass">

    <!-- fav -->

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <!-- title -->
    <title>Главная страница</title>

    <!-- stylesheets -->
    <link rel="stylesheet" href="../../resources/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/slick.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/magnific-popup.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/normalize.css">
    <link rel="stylesheet" href="../../resources/assets/css/mean-menu.css">
    <link rel="stylesheet" href="../../resources/assets/css/default.css">
    <link rel="stylesheet" href="../../resources/assets/css/style.css">



</head>



<body>
<!-- =============Preloader Starts=============-->
<%--<div class="loader">--%>
<%--    <div class="loding-cricle"></div>--%>
<%--</div>--%>
<!-- =============Preloader Ends=============-->


<!-- =================Header Area Starts================= -->

<header>
    <div class="header-area header-9 header-resposive pt-30 ">
        <div class="header-top">
            <div class="container">
                <div class="header-top">
                    <div class="row">
                        <div class="col-xl-3 col-lg-4 order-md-1 col-md-5   col-sm-12 col-12   ">
                            <ul class="user-info d-flex pt-15 justify-content-center justify-content-md-start  ">


                                <c:if test="${sessionScope.account != null}">

                                    <li><a href="/account">Личный кабинет</a></li>

                                    <li><a href="/logout">Выйти</a>
                                    </li>
                                </c:if>

                                <c:if test="${sessionScope.account == null}">
                                    <li><a href="/login">Войти</a>
                                    </li>
                                    <li><a href="/reg">Зарегистрироваться</a></li>

                                </c:if>

                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div class="header-menu mt-15 pt-25 pb-20">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-md-8">
                        <div class="main-menu ">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><h2>Главная страница</h2>

                                    </li>


                                </ul>
                            </nav>

                        </div>
                    </div>
                    <div class="col-xl-2 col-md-4">

                        <div class="search position-relative mr-15 ">

                            <div>
                                <form action="" method="get">
                                    <input id="findProduct" class="search-form" type="text" size="20" name="find"
                                           placeholder="Поиск....." >

                                    <button class="btn btn-search-form" type="submit">Найти</button>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="mobile-menu"></div>
            </div>
        </div>


    </div>
</header>


<div class="product-area  product-shop-page pt-50 ">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 pb-50">
                <div class="toolbar-navi d-inline-block plus ">
                    <div class="toolbar d-flex">

                        <div class="sort-by d-flex mr-30">
                            <span class="mr-10">Sort by:</span>
                            <form method="get">

                                    <select class="sort-by-option position-relative" name="filter">
                                        <!-- <select> -->
                                        <%--                                        <button id="sort-option" class="sort-option"><i class="fas fa-caret-down"></i>--%>
                                        <%--                                        </button>--%>

                                        <div id="sub-sort-option" class="sub-sort-option  position-absolute ">
                                            <!-- <a href="#">Alphbet</a>
                                            <a href="#">Price</a> -->
                                            <option>Выберите тип</option>

                                            <option class="sort-option  " size="20" value="electronic">
                                                Электроника
                                            </option>
                                            <option class="sort-option  " size="20" value="products_for_home">
                                                Товары для дома
                                            </option>
                                            <option class="sort-option  " size="20"  value="furniture">
                                                Мебель
                                            </option>
                                            <option class="sort-option  " size="20"  value="adornments">
                                                Украшения
                                            </option>


                                        </div>
                                        <!-- </select> -->

                                    </select>

                                <div>
                                    <button class="btn first" type="submit">Найти</button>
                                    <!-- <button type="submit" >Найти</button> -->
                                </div>
                            </form>


                        </div>

                    </div>
                </div>

            </div>

        </div>
        <div class="product-area product-shop-page  product-list-page  pt-50 ">
            <div class="container">
                <div class="row">
                    <c:forEach var="product" items="${requestScope.allProducts}">
                        <div class="col-xl-9 col-lg-9 col-md-12">

                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="product-sidebar">
                                        <div class="product-wrapper mb-30 d-flex border border-success p-2 mb-2 border-opacity-50 ">

                                            <div class="product-img pt-15 pl-20  pb-25   ">

                                                <div class="inner_container">
                                                <c:if test="${product.photo!=null}">
                                                    <img src="${pageContext.request.contextPath}/storage/${product.photo}"
                                                         width="150"
                                                         height="150"/>
                                                </c:if>
                                                </div>

                                            </div>

                                            <div class="product-detalis pt-15 pl-20  pb-25">

                                                <div class="inner_container dop ">
                                                    <label>

                                                        <ul>


                                                            <li><c:out value="${product.name}"/></li>


                                                            <li>Описание товара: <c:out value="${product.description}"/></li>

                                                            <li> Цена: <c:out value="${product.price}"/></li>
                                                            <li> Количество товара: <c:out value="${product.count}"/></li>
                                                            <li>Дата добавления объявления: <c:out value="${product.date}"/></li>

                                                        </ul>
                                                        <br>
                                                        <form>
                                                            <input type="submit" formmethod="get"
                                                                   formaction="<c:url value='/this_product'/>" hidden
                                                                   name="id" value="${product.id}"/>
                                                        </form>



                                                    </label>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>


    </div>
</div>


<!-- scripts -->
<script src="../../resources/assets/js/vendor/jquery.min.js"></script>
<script src="../../resources/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<script src="../../resources/assets/js/vendor/bootstrap.min.js"></script>
<script src="../../resources/assets/js/vendor/popper.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery-mean-menu.min.js"></script>
<script src="../../resources/assets/js/vendor/owl.carousel.min.js"></script>
<script src="../../resources/assets/js/vendor/isotope.pkgd.min.js"></script>
<script src="../../resources/assets/js/countdown.js"></script>
<script src="../../resources/assets/js/vendor/jquery.nice-number.js"></script>
<script src="../../resources/assets/js/vendor/slick.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="../../resources/assets/js/vendor/wow-1.3.0.min.js"></script>
<script src="../../resources/assets/js/main.js"></script>

</body>


</html>

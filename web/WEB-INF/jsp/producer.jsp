<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 12.10.2022
  Time: 3:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Producer</title>
    <script defer type="text/javascript" src="../../resources/jquery-3.6.1.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
    <script src="https://malsup.github.io/jquery.form.js"></script>

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <!-- title -->


    <!-- stylesheets -->
    <link rel="stylesheet" href="../../resources/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/slick.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/magnific-popup.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/normalize.css">
    <link rel="stylesheet" href="../../resources/assets/css/mean-menu.css">
    <link rel="stylesheet" href="../../resources/assets/css/default.css">
    <link rel="stylesheet" href="../../resources/assets/css/style.css">


</head>
<body>

<div class="loader">
    <div class="loding-cricle"></div>
</div>


<header>
    <div class="header-area header-9 header-resposive pt-30 ">

        <div class="header-top">
            <div class="container">
                <div class="header-top">
                    <div class="row">
                        <div class="col-xl-3 col-lg-4 order-md-1 col-md-5   col-sm-12 col-12   ">
                            <ul class="user-info d-flex pt-15 justify-content-center justify-content-md-start  ">
                                <%--                                <li><a href="/account">Личный кабинет</a></li>--%>
                                <%--                                <li><a href="/login">Войти</a></li>--%>
                                <%--                                <li><a href="/reg">Зарегистрироваться</a></li>--%>


                                <c:if test="${sessionScope.account != null}">

                                    <li><a href="/account">Личный кабинет</a></li>

                                    <li><a href="/logout">Выйти</a>
                                    </li>
                                </c:if>

                                <c:if test="${sessionScope.account == null}">
                                    <li><a href="/login">Войти</a>
                                    </li>
                                    <li><a href="/reg">Зарегистрироваться</a></li>

                                </c:if>

                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="header-menu mt-15 pt-25 pb-20">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-md-10">
                        <div class="main-menu ">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="/home_page">Главная страница</a></li>


                                </ul>
                            </nav>

                        </div>
                    </div>


                </div>
                <div class="mobile-menu"></div>
            </div>
        </div>
        <div class="page-title-area pt-20 pb-20 " style="background-image: url(assets/img/bg/contact-bg.png);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page-titel-detalis  ">
                            <div class="page-title position-relative">
                                <h2>Информация о товаре</h2>
                            </div>
                            <div class="page-bc">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/home_page"> <i
                                                class="fas fa-home "></i>Главная страница</a></li>
                                        <li class="breadcrumb-item active" aria-current="page"><a
                                                href="/this_product">Товар</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</header>

<div class="product-area product-shop-page  product-list-page product-detalis-page  pt-50 ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-8 col-11 col offset-xl-1 offset-lg-1">
                <div class=" slider-nav-thumbnails product-list-active d-md-none">

                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 col-lg-7 col-md-6 col-sm-12">
                <div class="product-list-slider">
                    <div class="product-img">
                        <c:if test="${requestScope.producer.photo != null}">
                            <img src="${pageContext.request.contextPath}/storage/${requestScope.producer.photo}" width="50%">
                        </c:if>
                        <c:if test="${requestScope.producer.photo==null}">
                            <img src="../../../resources/assets/img/default_img.jpg" width="50%">
                        </c:if>
                    </div>

                </div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12">
                <div class="product-wrapper product-wrapper-2 pt-60">
                    <div class="product-detalis">

                       <c:out value="${requestScope.producer.firstName}"/><br>
                        <c:out value="${requestScope.producer.lastName}"/><br>

                        <c:out value="${requestScope.producer.email}"/><br>



                        <div>
                            <form method="get" action="<c:url value='/account/chat'/>">
                                <input type="text" hidden name="producer_id" value="${requestScope.producer.id}"/>
                                <button class="btn first" type="submit" >Перейти в чат с продавцом</button>
                            </form>




                        </div>
                        <hr>


                    </div>
                </div>
            </div>
        </div>


    </div>
</div>



<script src="../../resources/assets/js/vendor/jquery.min.js"></script>
<script src="../../resources/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<script src="../../resources/assets/js/vendor/bootstrap.min.js"></script>
<script src="../../resources/assets/js/vendor/popper.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery-mean-menu.min.js"></script>
<script src="../../resources/assets/js/vendor/owl.carousel.min.js"></script>
<script src="../../resources/assets/js/vendor/isotope.pkgd.min.js"></script>
<script src="../../resources/assets/js/countdown.js"></script>
<script src="../../resources/assets/js/vendor/jquery.nice-number.js"></script>
<script src="../../resources/assets/js/vendor/slick.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="../../resources/assets/js/vendor/wow-1.3.0.min.js"></script>
<script src="../../resources/assets/js/main.js"></script>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 11.10.2022
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="shop, ecommerce, store, multipurpose, shopify, woocommerce, html5, css3, sass">


    <!-- title -->
    <title>Product</title>

    <!-- stylesheets -->
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/slick.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/magnific-popup.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/normalize.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/jquery.nice-number.css">
    <link rel="stylesheet" href="../../../resources/assets/css/mean-menu.css">
    <link rel="stylesheet" href="../../../resources/assets/css/default.css">
    <link rel="stylesheet" href="../../../resources/assets/css/style.css">


</head>
<body>

<header>
    <div class="header-area header-9 header-resposive pt-30 ">

        <div class="header-menu mt-15 pt-25 pb-20">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-md-10">
                        <div class="main-menu ">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="/home_page">Главная страница</a></li>


                                    <c:if test="${sessionScope.account != null}">

                                        <li><a href="/account">Личный кабинет</a></li>

                                        <li><a href="/logout">Выйти</a></li>
                                    </c:if>

                                    <c:if test="${sessionScope.account == null}">
                                        <li><a href="/login">Войти</a>
                                        </li>
                                        <li><a href="/reg">Зарегистрироваться</a></li>

                                    </c:if>


                                </ul>
                            </nav>

                        </div>
                    </div>


                </div>
                <div class="mobile-menu"></div>
            </div>
        </div>


    </div>
</header>

<!-- =================Header Area Ends================= -->

<!-- =================Page Title Area Starts================= -->

<div class="page-title-area pt-20 pb-20 " style="background-image: url(assets/img/bg/contact-bg.png);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-titel-detalis  ">
                    <div class="page-title position-relative">
                        <h2>Информация о товаре</h2>
                    </div>
                    <div class="page-bc">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/home_page"> <i
                                        class="fas fa-home "></i>Главная страница</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a
                                        href="/this_product">Товар</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- =================Page Title Area Starts================= -->

<!-- =================Product Area Starts================= -->

<div class="product-area product-shop-page  product-list-page product-detalis-page  pt-50 ">
    <div class="container">

        <div class="row">
            <div class="col-xl-6 col-lg-7 col-md-6 col-sm-12">
                <div class="product-list-slider">
                    <div class="product-img">
                        <c:forEach var="photo" items="${requestScope.product.allPhotos}">

                            <%--    <c:out value="${photo}"/>--%>
                            <c:if test="${photo != null}">
                                <img src="${pageContext.request.contextPath}/storage/${photo}" width="50%">
                            </c:if>

                        </c:forEach>
                    </div>

                </div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12">
                <div class="product-wrapper product-wrapper-2 pt-10">
                    <p class="product-detalis">


                    <h2><c:out value="${requestScope.product.name}"/></h2>


                    Описание товара: <c:out value="${requestScope.product.description}"/><br>

                    Цена товара: <c:out value="${requestScope.product.price}"/><br>
                    Количество товара: <c:out value="${requestScope.product.count}"/><br>

                    Дата добавления объявления: <c:out value="${requestScope.product.date}"/><br>

                    <br>
                    Продавец: <c:out value="${requestScope.accountFirstName}"/> <c:out
                        value="${requestScope.accountLastName}"/>
                    <br>
                    <p>

                    <form method="get" action="<c:url value='/producer'/>">
                        <input type="text" hidden name="producer_id" value="${requestScope.product.accountId}"/>
                        <%--                            <input type="submit" value="Show more..."/>--%>

                            <button class="btn first seven" type="submit">Перейти на страницу продавца</button>

                    </form>

                    </p>


                    <p>
                    <form method="post" action="<c:url value='/order'/>">
                        <input type="number" hidden name="product_id" value="${requestScope.product.id}"/>
                        <button class="btn first eleven" type="submit">Купить</button>
                    </form>
                    </p>


                    <hr>


                </div>
            </div>
        </div>
    </div>


</div>



<script src="../../../resources/assets/js/vendor/jquery.min.js"></script>
<script src="../../../resources/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<script src="../../../resources/assets/js/vendor/bootstrap.min.js"></script>
<script src="../../../resources/assets/js/vendor/popper.min.js"></script>
<script src="../../../resources/assets/js/vendor/jquery-mean-menu.min.js"></script>
<script src="../../../resources/assets/js/vendor/owl.carousel.min.js"></script>
<script src="../../../resources/assets/js/vendor/isotope.pkgd.min.js"></script>
<script src="../../../resources/assets/js/countdown.js"></script>
<script src="../../../resources/assets/js/vendor/jquery.nice-number.js"></script>
<script src="../../../resources/assets/js/vendor/slick.min.js"></script>
<script src="../../../resources/assets/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="../../../resources/assets/js/vendor/wow-1.3.0.min.js"></script>
<script src="../../../resources/assets/js/main.js"></script>
</body>
</html>

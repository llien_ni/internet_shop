<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 11.10.2022
  Time: 0:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="shop, ecommerce, store, multipurpose, shopify, woocommerce, html5, css3, sass">

    <!-- fav -->

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- title -->
    <title>Создать объявление</title>

    <!-- stylesheets -->
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/slick.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/magnific-popup.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/normalize.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/jquery.nice-number.css">
    <link rel="stylesheet" href="../../../resources/assets/css/mean-menu.css">
    <link rel="stylesheet" href="../../../resources/assets/css/default.css">
    <link rel="stylesheet" href="../../../resources/assets/css/style.css">



</head>
<body>

<div class="loader">
    <div class="loding-cricle"></div>
</div>
<!-- =============Preloader Ends=============-->


<!-- =================Header Area Starts================= -->

<header>
    <div class="header-area header-9 header-resposive pt-30 ">
        <div class="header-top">
            <div class="container">
                <div class="header-top">
                    <div class="row">
                        <div class="col-xl-3 col-lg-4 order-md-1 col-md-5   col-sm-12 col-12   ">
                            <ul class="user-info d-flex pt-15 justify-content-center justify-content-md-start  ">
                                <li><a href="/account">My Account</a></li>

                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div class="header-menu mt-15 pt-25 pb-20">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-md-10">
                        <div class="main-menu ">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="/home_page">Home</a>

                                    </li>


                                </ul>
                            </nav>

                        </div>
                    </div>

                </div>
                <div class="mobile-menu"></div>
            </div>
        </div>


    </div>
</header>

<!-- =================Header Area Ends================= -->

<!-- =================Page Title Area Starts================= -->

<div class="page-title-area pt-10 pb-10 " style="background-image: url(assets/img/bg/chechout-page-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-titel-detalis  ">
                    <div class="page-title position-relative">
                        <h2>Добавление продукта</h2>
                    </div>
                    <div class="page-bc">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/home_page"> <i
                                        class="fas fa-home "></i>Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a
                                        href="/reg">Sign Up Page</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- =================Page Title Area End================= -->
<!-- =================Login Area Starts================= -->

<div class="login-page-area pt-50">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-9">
                <div class="login-detalis pt-40 pr-40 pl-40 pb-40">
                    <div class="login-input">
                        <form id="form" action="" method="post" enctype="multipart/form-data">


                            <div>
                                <input class="required content" type="text" required placeholder="Название" name="name"><br>
                                <label class="messages error"></label>
                            </div>
                            <div>
                                <input class="required content" type="text" required placeholder="Описание"
                                       name="description"><br>
                                <label class="messages error"></label>
                            </div>
                            <div>
                                <input class="required num" type="text" required placeholder="Количество"
                                       name="count"><br>
                                <label class="messages error"></label>
                            </div>

                            <div>
                                <input class="required  num" type="text" required placeholder="Цена"
                                       name="price"><br>
                                <label class="messages error"></label>
                            </div>
                            <div>
                                <select class="sort-by-option position-relative" name="product_type">

                                    <div id="sub-sort-option" class="sub-sort-option  position-absolute ">
                                        <!-- <a href="#">Alphbet</a>
                                        <a href="#">Price</a> -->
                                        <option>Выберите тип</option>

                                        <option class="sort-option  " size="20" value="electronic">
                                            Электроника
                                        </option>
                                        <option class="sort-option  " size="20" value="products_for_home">
                                            Товары для дома
                                        </option>
                                        <option class="sort-option  " size="20" value="furniture">
                                            Мебель
                                        </option>
                                        <option class="sort-option  " size="20" value="adornments">
                                            Украшения
                                        </option>


                                    </div>
                                    <!-- </select> -->

                                </select>



<%--                                <input class="required content" type="text" required placeholder="Тип товара"--%>
<%--                                       name="product_type"><br>--%>
<%--                                <label class="messages error"></label>--%>
                            </div>

                            <br>
                            <label>
                                Выберите файл для загрузки: <input type="file" name="file" value="file" multiple>

                            </label>



                            <div class="login-button text-center pt-30">
                                <button class="btn first button-for-login five" type="submit" >Add</button>
<%--                                <a type="submit">Add</a>--%>
                            </div>

                        </form>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('document').ready(function () {
        $('#form').validate(
            {
                // правила для проверки
                rules: {
                    content: {
                        required: true,
                        minlength: 2,
                        maxlength: 30
                    },
                    num: {
                        required: true,
                        minlength: 1
                    }
                },

                // выводимые сообщения при нарушении соответствующих правил
                messages: {
                    "name": {
                        required: "Добавьте название",
                        minlength: "От 2 до 30 символов",
                        maxlength: "От 2 до 30 символов"
                    },
                    "description": {
                        required: "Добавьте описание",

                    },
                    "count": {
                        required: "Добавьте количество",

                    },
                    "product_type": {
                        required: "Добавьте тип продукта",
                        minlength: "От 2 до 30 символов",
                        maxlength: "От 2 до 30 символов"
                    },
                    "price": {
                        required: "Добавьте цену",

                    },

                },


                // указаваем обработчик
                submitHandler: function (form) {
                    $(form).ajaxSubmit({
                        target: '#preview',

                    })
                }
            });
    });
</script>


<script src="../../../resources/assets/js/vendor/jquery.min.js"></script>
<script src="../../../resources/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<script src="../../../resources/assets/js/vendor/bootstrap.min.js"></script>
<script src="../../../resources/assets/js/vendor/popper.min.js"></script>
<script src="../../../resources/assets/js/vendor/jquery-mean-menu.min.js"></script>
<script src="../../../resources/assets/js/vendor/owl.carousel.min.js"></script>
<script src="../../../resources/assets/js/vendor/isotope.pkgd.min.js"></script>
<script src="../../../resources/assets/js/countdown.js"></script>
<script src="../../../resources/assets/js/vendor/jquery.nice-number.js"></script>
<script src="../../../resources/assets/js/vendor/slick.min.js"></script>
<script src="../../../resources/assets/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="../../../resources/assets/js/vendor/wow-1.3.0.min.js"></script>
<script src="../../../resources/assets/js/main.js"></script>
</body>
</html>
<%--select--%>
<%--    option--%>
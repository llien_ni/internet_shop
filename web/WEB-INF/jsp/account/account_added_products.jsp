<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 11.10.2022
  Time: 0:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="shop, ecommerce, store, multipurpose, shopify, woocommerce, html5, css3, sass">

    <!-- fav -->


    <!-- title -->


    <!-- stylesheets -->
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/slick.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/magnific-popup.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/normalize.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/jquery.nice-number.css">
    <link rel="stylesheet" href="../../../resources/assets/css/mean-menu.css">
    <link rel="stylesheet" href="../../../resources/assets/css/default.css">
    <link rel="stylesheet" href="../../../resources/assets/css/style.css">

    <title>Products</title>



</head>




<div class="loader">
    <div class="loding-cricle"></div>
</div>
<!-- =============Preloader Ends=============-->


<!-- =================Header Area Starts================= -->


<header>
    <div class="header-area header-9 header-resposive pt-30 ">

        <div class="header-menu mt-15 pt-25 pb-20">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-md-10">
                        <div class="main-menu ">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="${pageContext.request.contextPath}/home_page">Главная страница</a></li>

                                    <li><a href="${pageContext.request.contextPath}/account">Личный кабинет</a></li>


                                    <li><a href="${pageContext.request.contextPath}/account/orders">Мои заказы</a></li>
                                    <li><a href="${pageContext.request.contextPath}/account/chats">Messenger</a>

                                    </li>


                                </ul>
                            </nav>

                        </div>
                    </div>
                    <div class="col-xl-2 col-md-2">
                        <div class="site-info d-flex justify-content-end">
                            <div class="search position-relative mr-15 ">
                                <a href="${pageContext.request.contextPath}/logout">Выйти</a>

                            </div>


                        </div>
                    </div>

                </div>
                <div class="mobile-menu"></div>
            </div>
        </div>


    </div>
</header>
<body>
<!-- =================Header Area Ends================= -->

<!-- =================Page Title Area Starts================= -->

<div class="page-title-area pt-20 pb-20 " style="background-image: url(assets/img/bg/contact-bg.png);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-titel-detalis  ">
                    <div class="page-title position-relative">
                        <h2>Messenger</h2>
                    </div>
                    <div class="page-bc">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/home_page"> <i
                                        class="fas fa-home "></i>Главная страница</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a
                                        href="${pageContext.request.contextPath}/account">Личный кабинет</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a
                                        href="${pageContext.request.contextPath}/account/products">Мои товары</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="product-area  product-shop-page pt-50 ">
    <div class="container">

        <div class="product-area product-shop-page  product-list-page  pt-50 ">
            <div class="container">
                <form action="/account/add_product" method="get">
                    <button class="btn first" type="submit" value="add">Добавить</button>
<%--                    <input type="button" value="add" onClick='location.href="add_product"'>--%>
                </form>
                <div class="row">
                    <c:forEach var="product" items="${requestScope.productsAddedThisAccount}">
                        <div class="col-xl-9 col-lg-9 col-md-12">


                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="product-sidebar">
                                        <div class="product-wrapper mb-30 d-flex border border-success p-2 mb-2 border-opacity-50 ">
                                            <div class="product-img pt-15 pl-20  pb-25   ">
                                                <div class="inner_container">
                                                <c:if test="${product.photo!=null}">
                                                    <img src="${pageContext.request.contextPath}/storage/${product.photo}" width="150" height="150"/>

                                                </c:if>
                                                </div>
                                            </div>
                                            <div class="product-detalis pt-15 pl-20  pb-25">

                                                <div class="inner_container dop ">
                                                    <label>
                                                        <br>
<%--                                                        <form>--%>
<%--                                                            <input type="submit" formmethod="get" formaction="<c:url value='/this_product'/>" hidden--%>
<%--                                                                   name="id" value="${product.id}"/>--%>
<%--                                                        </form>--%>

                                                        <ul>
                                                            <li><c:out value="${product.name}"/></li>

                                                            Тип товара: <li><c:out value="${product.productType}"/></li>
                                                            Описание товара: <li><c:out value="${product.description}"/></li>

                                                            Цена: <li> <c:out value="${product.price}"/></li>
                                                            Количество товара: <li> <c:out value="${product.count}"/></li>
                                                            Дата добавления объявления: <li><c:out value="${product.date}"/></li>

                                                        </ul>
                                                        <br>

                                                        <form method="post" action="<c:url value='/account/delete_product'/>">
                                                            <input type="number" hidden name="deleteId" value="${product.id}"/>


                                                            <button class="btn first" type="submit" >Удалить товар</button>
                                                        </form>

                                                        <form method="get" action="<c:url value='/account/update_product'/>">
                                                            <input type="number" hidden name="id" value="${product.id}"/>
                                                            <button class="btn first" type="submit" >Обновить товар</button>
                                                        </form>




                                                    </label>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>


    </div>
</div>







<!-- scripts -->
<script src="../../../resources/assets/js/vendor/jquery.min.js"></script>
<script src="../../../resources/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<script src="../../../resources/assets/js/vendor/bootstrap.min.js"></script>
<script src="../../../resources/assets/js/vendor/popper.min.js"></script>
<script src="../../../resources/assets/js/vendor/jquery-mean-menu.min.js"></script>
<script src="../../../resources/assets/js/vendor/owl.carousel.min.js"></script>
<script src="../../../resources/assets/js/vendor/isotope.pkgd.min.js"></script>
<script src="../../../resources/assets/js/countdown.js"></script>
<script src="../../../resources/assets/js/vendor/jquery.nice-number.js"></script>
<script src="../../../resources/assets/js/vendor/slick.min.js"></script>
<script src="../../../resources/assets/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="../../../resources/assets/js/vendor/wow-1.3.0.min.js"></script>
<script src="../../../resources/assets/js/main.js"></script>


</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 16.10.2022
  Time: 21:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="shop, ecommerce, store, multipurpose, shopify, woocommerce, html5, css3, sass">

    <!-- fav -->

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">


    <!-- stylesheets -->
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/slick.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/magnific-popup.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/normalize.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/jquery.nice-number.css">
    <link rel="stylesheet" href="../../../resources/assets/css/mean-menu.css">
    <link rel="stylesheet" href="../../../resources/assets/css/default.css">
    <link rel="stylesheet" href="../../../resources/assets/css/style.css">
    <title>Update</title>



</head>
<body>



<header>
    <div class="header-area header-9 header-resposive pt-30 ">

        <div class="header-menu mt-15 pt-25 pb-20">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-md-10">
                        <div class="main-menu ">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="/home_page">Главная страница</a></li>


                                    <li><a href="/account/products">Мои товары</a>

                                    </li>
                                    <li><a href="/account/orders">Мои заказы</a></li>
                                    <li><a href="/account/chats">Мои чаты</a></li>

                                </ul>
                            </nav>

                        </div>
                    </div>
                    <div class="col-xl-2 col-md-2">
                        <div class="site-info d-flex justify-content-end">
                            <div class="search position-relative mr-15 ">
                                <a href="/logout">Выйти</a>

                            </div>


                        </div>
                    </div>

                </div>
                <div class="mobile-menu"></div>
            </div>
        </div>


    </div>
</header>

<!-- =================Header Area Ends================= -->

<!-- =================Page Title Area Starts================= -->

<div class="page-title-area pt-20 pb-20 " style="background-image: url(assets/img/bg/contact-bg.png);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-titel-detalis  ">
                    <div class="page-title position-relative">
                        <h2>Личный кабинет</h2>
                    </div>
                    <div class="page-bc">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/home-page"> <i
                                        class="fas fa-home "></i>Главная страница</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a
                                        href="/account">Личный кабинет</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- =================Page Title Area Starts================= -->

<!-- =================Product Area Starts================= -->

<div class="product-area product-shop-page  product-list-page product-detalis-page  pt-50 ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-8 col-11 col offset-xl-1 offset-lg-1">
                <div class=" slider-nav-thumbnails product-list-active d-md-none">
                    <%--                    <a class="active"><img src="assets/img/product/product-62.png" alt=""></a>--%>


                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 col-lg-7 col-md-6 col-sm-12">
                <div class="product-list-slider">


                    <c:if test="${requestScope.account.photo!=null}">
                        <img src="${pageContext.request.contextPath}/storage/${requestScope.account.photo}" width="200"
                             height="200">
                    </c:if>


                </div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12">
                <div class="product-wrapper product-wrapper-2 pt-60">
                    <%--                    <div class="product-detalis">--%>


                    <div class="login-detalis">
                        <form style="" action="" method="post">
                            <div class="login-input">

                                <%--        <input type="number" hidden name="id" value="${requestScope.project.id}"/>--%>


                                <div>
                                    <input class="required content" type="text" size="40" name="firstName"
                                           value="${requestScope.account.firstName}">
                                </div>
                                <div>
                                    <input class="required content" type="text" size="40" name="lastName"
                                           value="${requestScope.account.lastName}">
                                </div>
                                <div>

                                    <input class="required content" type="text" size="40" name="age"
                                           value="${requestScope.account.age}">
                                </div>


                                <br>
                                <div>
                                    <button class="btn first second" type="submit">Обновить</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

                <form method="post" action="/account/delete_account_photo">
                    <input type="text" hidden name="deletePhoto" value="${requestScope.account.photo}">

                    <button class="btn first third" type="submit">Удалить фото</button>


                </form>

                <%--    enctype-это то в каком формате данные будут отправлены на сервер--%>
                <form method="post" enctype="multipart/form-data" action="/account/upload">
                    Выберите файл для загрузки: <input type="file" name="file" value="file">
                    <input type="text" hidden name="account_id" value="${sessionScope.account.id}">
                    <br>
                    <button class="btn first forth" type="submit">Загрузить фото</button>

                </form>


            </div>


        </div>
    </div>
</div>
</div>


</div>


<!-- scripts -->
<script src="../../../resources/assets/js/vendor/jquery.min.js"></script>
<script src="../../../resources/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<script src="../../../resources/assets/js/vendor/bootstrap.min.js"></script>
<script src="../../../resources/assets/js/vendor/popper.min.js"></script>
<script src="../../../resources/assets/js/vendor/jquery-mean-menu.min.js"></script>
<script src="../../../resources/assets/js/vendor/owl.carousel.min.js"></script>
<script src="../../../resources/assets/js/vendor/isotope.pkgd.min.js"></script>
<script src="../../../resources/assets/js/countdown.js"></script>
<script src="../../../resources/assets/js/vendor/jquery.nice-number.js"></script>
<script src="../../../resources/assets/js/vendor/slick.min.js"></script>
<script src="../../../resources/assets/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="../../../resources/assets/js/vendor/wow-1.3.0.min.js"></script>
<script src="../../../resources/assets/js/main.js"></script>


</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 12.10.2022
  Time: 16:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="shop, ecommerce, store, multipurpose, shopify, woocommerce, html5, css3, sass">

    <!-- fav -->

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- title -->


    <title>Chats</title>
    <!-- stylesheets -->
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/slick.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/magnific-popup.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/normalize.css">
    <link rel="stylesheet" href="../../../resources/assets/css/vendor/jquery.nice-number.css">
    <link rel="stylesheet" href="../../../resources/assets/css/mean-menu.css">
    <link rel="stylesheet" href="../../../resources/assets/css/default.css">
    <link rel="stylesheet" href="../../../resources/assets/css/style.css">


</head>
<body>


<!-- =============Preloader Ends=============-->


<!-- =================Header Area Starts================= -->

<header>
    <div class="header-area header-9 header-resposive pt-30 ">

        <div class="header-menu mt-15 pt-25 pb-20">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-md-10">
                        <div class="main-menu ">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="/home_page">Главная страница</a></li>

                                    <li><a href="/account">Личный кабинет</a></li>

                                    <li><a href="/account/products">Мои товары</a>

                                    </li>
                                    <li><a href="/account/orders">Мои заказы</a></li>


                                </ul>
                            </nav>

                        </div>
                    </div>
                    <div class="col-xl-2 col-md-2">
                        <div class="site-info d-flex justify-content-end">
                            <div class="search position-relative mr-15 ">
                                <a href="/logout">Выйти</a>

                            </div>


                        </div>
                    </div>

                </div>
                <div class="mobile-menu"></div>
            </div>
        </div>


    </div>
</header>

<!-- =================Header Area Ends================= -->

<!-- =================Page Title Area Starts================= -->

<div class="page-title-area pt-20 pb-20 " style="background-image: url(assets/img/bg/contact-bg.png);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-titel-detalis  ">
                    <div class="page-title position-relative">
                        <h2>Messenger</h2>
                    </div>
                    <div class="page-bc">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/home_page"> <i
                                        class="fas fa-home "></i>Главная страница</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a
                                        href="/account">Личный кабинет</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a
                                        href="/account/chats">Messenger</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="product-area  product-shop-page pt-10 ">
    <div class="container">

        <div class="product-area product-shop-page  product-list-page  pt-10 ">
            <div class="container">
                <div class="row">
                    <c:forEach var="account" items="${requestScope.otherAccountsWithThisHaveChat}">

                    <div class="col-xl-9 col-lg-9 col-md-12">

                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="product-sidebar">
                                        <div class="product-wrapper mb-30 d-flex">

                                            <div class="product-detalis pt-15 pl-20  pb-25">

                                                <div class="inner_container border border-success p-2 mb-2 border-opacity-50">
                                                    <label>
                                                        <form method="get">
                                                            <input type="submit" formaction="<c:url value='/account/chat'/>" hidden
                                                                   name="producer_id" value="${account.id}"/>

                                                        </form>

                                                        <ul>
                                                            <c:out value="${account.firstName}"/> <c:out value="${account.lastName}"/><br>
                                                        </ul>
                                                    </label>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>


    </div>
</div>

















<!-- scripts -->
<script src="../../../resources/assets/js/vendor/jquery.min.js"></script>
<script src="../../../resources/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<script src="../../../resources/assets/js/vendor/bootstrap.min.js"></script>
<script src="../../../resources/assets/js/vendor/popper.min.js"></script>
<script src="../../../resources/assets/js/vendor/jquery-mean-menu.min.js"></script>
<script src="../../../resources/assets/js/vendor/owl.carousel.min.js"></script>
<script src="../../../resources/assets/js/vendor/isotope.pkgd.min.js"></script>
<script src="../../../resources/assets/js/countdown.js"></script>
<script src="../../../resources/assets/js/vendor/jquery.nice-number.js"></script>
<script src="../../../resources/assets/js/vendor/slick.min.js"></script>
<script src="../../../resources/assets/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="../../../resources/assets/js/vendor/wow-1.3.0.min.js"></script>
<script src="../../../resources/assets/js/main.js"></script>


</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 08.10.2022
  Time: 22:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Registration</title>
    <script defer type="text/javascript" src="../../resources/jquery-3.6.1.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
    <script src="https://malsup.github.io/jquery.form.js"></script>

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <!-- title -->


    <!-- stylesheets -->
    <link rel="stylesheet" href="../../resources/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/slick.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/magnific-popup.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/normalize.css">
    <link rel="stylesheet" href="../../resources/assets/css/mean-menu.css">
    <link rel="stylesheet" href="../../resources/assets/css/default.css">
    <link rel="stylesheet" href="../../resources/assets/css/style.css">



    <style>

        .button-reg{
            position: relative;
            left: 0px;
            bottom: 40px;
        }
    </style>


</head>
<body>
<!-- =============Preloader Starts=============-->
<div class="loader">
    <div class="loding-cricle"></div>
</div>
<!-- =============Preloader Ends=============-->





<!-- =================Header Area Starts================= -->

<header>
    <div class="header-area header-9 header-resposive pt-30 ">
        <div class="header-top">
            <div class="container">
                <div class="header-top">
                    <div class="row">
                        <div class="col-xl-3 col-lg-4 order-md-1 col-md-5   col-sm-12 col-12   ">
                            <ul class="user-info d-flex pt-15 justify-content-center justify-content-md-start  ">
                                <li><a href="/account">My Account</a></li>

                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div class="header-menu mt-15 pt-25 pb-20">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-md-10">
                        <div class="main-menu ">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="/home_page">Home</a>

                                    </li>






                                </ul>
                            </nav>

                        </div>
                    </div>

                </div>
                <div class="mobile-menu"></div>
            </div>
        </div>


    </div>
</header>

<!-- =================Header Area Ends================= -->

<!-- =================Page Title Area Starts================= -->

<div class="page-title-area pt-10 pb-10 " style="background-image: url(assets/img/bg/chechout-page-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-titel-detalis  ">
                    <div class="page-title position-relative">
                        <h2>Sign Up Page</h2>
                    </div>
                    <div class="page-bc">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/home_page"> <i
                                        class="fas fa-home "></i>Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a
                                        href="/reg">Sign Up Page</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- =================Page Title Area End================= -->
<!-- =================Login Area Starts================= -->

<div class="login-page-area pt-50">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-9">
                <div class="login-detalis pt-40 pr-40 pl-40 pb-40">
                    <div class="login-input">
                        <form id="form" method="post" action="">

                            <div >
                                <input class="required content" type="text" required placeholder="first_name" name="first_name"><br>
                                <label class="messages error"></label>
                            </div>
                            <div >
                                <input class="required content" type="text" required placeholder="last_name" name="last_name"><br>
                                <label class="messages error"></label>
                            </div>
                            <div >
                                <input class="required content" type="text" required placeholder="age" name="age"><br>
                                <label class="messages error"></label>
                            </div>

                            <div >
                                <input class="required content" type="text" required placeholder="email" name="email"><br>
                                <label class="messages error"></label>
                            </div>
                            <div >
                                <input class="required password" type="password" required placeholder="password" name="password"><br>
                                <label class="messages error"></label>
                            </div>


                            <label class="messages error">
                                <c:if test="${requestScope.error != null}">
                                    <c:out value="${requestScope.error}"/>
                                </c:if>
                            </label>

                            <div class="login-button text-center pt-30 pb-10 ">
                                <button class="btn first button-reg " type="submit" >Sign Up</button>
                            </div>

                        </form>

                    </div>




                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('document').ready(function(){
        $('#form').validate(
            {
                // правила для проверки
                rules:{
                    content: {
                        required: true,
                        minlength: 2,
                        maxlength: 30
                    },
                    password: {
                        required: true,
                        minlength: 2,
                        maxlength: 30
                    }
                },

                // выводимые сообщения при нарушении соответствующих правил
                messages:{
                    "first_name":{
                        required: "Это поле не может быть пустым",
                        minlength: "От 2 до 30 символов",
                        maxlength: "От 2 до 30 символов"
                    },
                    "last_name":{
                        required: "Это поле не может быть пустым",
                        minlength: "От 2 до 30 символов",
                        maxlength: "От 2 до 30 символов"
                    },
                    "age":{
                        required: "Это поле не может быть пустым",
                        minlength: "От 2 до 30 символов",
                        maxlength: "От 2 до 30 символов"
                    },
                    "email":{
                        required: "Это поле не может быть пустым",
                        minlength: "От 2 до 30 символов",
                        maxlength: "От 2 до 30 символов"
                    },
                    "password":{
                        required: "Это поле не может быть пустым",
                        minlength: "От 2 до 30 символов",
                        maxlength: "От 2 до 30 символов"
                    },

                },


                // указаваем обработчик
                submitHandler: function(form){
                    $(form).ajaxSubmit({
                        target: '#preview',

                    })
                }
            });
    });
</script>
<script src="../../resources/assets/js/vendor/jquery.min.js"></script>
<script src="../../resources/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<script src="../../resources/assets/js/vendor/bootstrap.min.js"></script>
<script src="../../resources/assets/js/vendor/popper.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery-mean-menu.min.js"></script>
<script src="../../resources/assets/js/vendor/owl.carousel.min.js"></script>
<script src="../../resources/assets/js/vendor/isotope.pkgd.min.js"></script>
<script src="../../resources/assets/js/countdown.js"></script>
<script src="../../resources/assets/js/vendor/jquery.nice-number.js"></script>
<script src="../../resources/assets/js/vendor/slick.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="../../resources/assets/js/vendor/wow-1.3.0.min.js"></script>
<script src="../../resources/assets/js/main.js"></script>
</body>
</html>

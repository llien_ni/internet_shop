<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 12.10.2022
  Time: 1:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <title>Order</title>
    <title>Title</title>
    <script defer type="text/javascript" src="../../resources/jquery-3.6.1.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
    <script src="https://malsup.github.io/jquery.form.js"></script>
    <%--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"--%>
    <%--          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">--%>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>


    <link rel="stylesheet" href="../../resources/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/slick.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/magnific-popup.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/normalize.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/jquery.nice-number.css">
    <link rel="stylesheet" href="../../resources/assets/css/mean-menu.css">
    <link rel="stylesheet" href="../../resources/assets/css/default.css">
    <link rel="stylesheet" href="../../resources/assets/css/style.css">
</head>
<body>
<div class="loader">
    <div class="loding-cricle"></div>
</div>


<header>
    <div class="header-area header-9 header-resposive pt-30 ">

        <div class="header-menu mt-15 pt-25 pb-20">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-md-10">
                        <div class="main-menu ">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="${pageContext.request.contextPath}/home_page">Главная страница</a></li>



                                </ul>
                            </nav>

                        </div>
                    </div>
                    <div class="col-xl-2 col-md-2">
                        <div class="site-info d-flex justify-content-end">
                            <div class="search position-relative mr-15 ">
                                <a href="${pageContext.request.contextPath}/logout">Выйти</a>

                            </div>


                        </div>
                    </div>

                </div>
                <div class="mobile-menu"></div>
            </div>
        </div>


    </div>
</header>

<!-- =================Header Area Ends================= -->

<!-- =================Page Title Area Starts================= -->

<div class="page-title-area pt-20 pb-20 " style="background-image: url(assets/img/bg/contact-bg.png);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-titel-detalis  ">
                    <div class="page-title position-relative">
                        <h2>Личный кабинет</h2>
                    </div>
                    <div class="page-bc">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/home_page"> <i
                                        class="fas fa-home "></i>Главная страница</a></li>

                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="login-page-area pt-50">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-9">
                <div class="login-detalis pt-40 pr-40 pl-40 pb-40">
                    <p>Нет доступа </p>

                </div>
            </div>
        </div>
    </div>
</div>



<!-- scripts -->
<!-- scripts -->
<script src="../../resources/assets/js/vendor/jquery.min.js"></script>
<script src="../../resources/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<script src="../../resources/assets/js/vendor/bootstrap.min.js"></script>
<script src="../../resources/assets/js/vendor/popper.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery-mean-menu.min.js"></script>
<script src="../../resources/assets/js/vendor/owl.carousel.min.js"></script>
<script src="../../resources/assets/js/vendor/isotope.pkgd.min.js"></script>
<script src="../../resources/assets/js/countdown.js"></script>
<script src="../../resources/assets/js/vendor/jquery.nice-number.js"></script>
<script src="../../resources/assets/js/vendor/slick.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="../../resources/assets/js/vendor/wow-1.3.0.min.js"></script>
<script src="../../resources/assets/js/main.js"></script>

</body>
</html>

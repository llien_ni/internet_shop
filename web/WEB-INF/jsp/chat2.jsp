<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 12.10.2022
  Time: 9:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Chat</title>
    <script defer type="text/javascript" src="../../resources/jquery-3.6.1.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
    <script src="https://malsup.github.io/jquery.form.js"></script>

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <!-- title -->


    <!-- stylesheets -->
    <link rel="stylesheet" href="../../resources/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/slick.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/magnific-popup.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/normalize.css">
    <link rel="stylesheet" href="../../resources/assets/css/mean-menu.css">
    <link rel="stylesheet" href="../../resources/assets/css/default.css">
    <link rel="stylesheet" href="../../resources/assets/css/style.css">


</head>


<body>

<header>
    <div class="header-area header-9 header-resposive pt-30 ">
        <div class="header-top">
            <div class="container">
                <div class="header-top">
                    <div class="row">
                        <div class="col-xl-3 col-lg-4 order-md-1 col-md-5   col-sm-12 col-12   ">
                            <ul class="user-info d-flex pt-15 justify-content-center justify-content-md-start  ">
                                <li><a href="/account">My Account</a></li>
                                <li><a href="/home_page">Главная страница</a></li>
                                <li><a href="/account">Личный кабинет</a></li>
                                <li><a href="/account/chats">Мои чаты</a></li>

                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="header-menu mt-15 pt-25 pb-20">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-md-10">

                    </div>
                    <div class="col-xl-2 col-md-2">
                        <div class="site-info d-flex justify-content-end">

                            <div class="react position-relative mr-15">
                                <a href="#"><i class="far fa-heart"></i></a>
                                <div class="badge">0</div>
                            </div>
                            <div class="cart position-relative mr-10">
                                <a href="#" id="mini-cart" class="mini-cart-hide"><img src="assets/img/icon/bag.png"
                                                                                       alt=""></a>
                                <!-- <button id="mini-cart"><img src="assets/img/icon/bag.png" alt=""></button> -->
                                <div class="badge">0</div>

                                <div id="cart-show" class="product-area product-shop-page mini-cart-product-page ">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="mobile-menu"></div>
            </div>
        </div>


    </div>
</header>

<!-- =================Header Area Ends================= -->

<!-- =================Page Title Area Starts================= -->

<div class="page-title-area pt-10 pb-10 " style="background-image: url(assets/img/bg/chechout-page-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-titel-detalis  ">
                    <div class="page-title position-relative">
                        <h2>Messenger</h2>
                    </div>
                    <div class="page-bc">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/home_page"> <i
                                        class="fas fa-home "></i>Home</a></li>
                                <li class="breadcrumb-item"><a href="/account"> <i
                                        class="fas fa-home "></i>My account</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a
                                        href="/account/chats">Messenger</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- =================Page Title Area End================= -->
<!-- =================Login Area Starts================= -->

<div class="login-page-area pt-50">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-9">

                <div class="login-detalis pt-40 pr-40 pl-40 pb-40">
                    <div>

                        <c:forEach var="message" items="${requestScope.messages}">


                            <h5><c:out value="${message.accountFirstName}"/> </h5>
                            <p><c:out value="${message.text}"/></p>


                        </c:forEach>

                    </div>



                    <form id="form" method="post" action="">
                        <div class="sentmessage">

                            <input class="search-form" type="text" required placeholder="Message..." name="message"/>
                        </div>

                        <div class="login-button text-center pt-30 pb-10">
                            <button class="btn first" type="submit">sent</button>
                        </div>


                    </form>



                </div>
            </div>
        </div>
    </div>
</div>


















<a href="/home_page">To main page</a><br>
Hello from chat
<div>

        <c:forEach var="message" items="${requestScope.messages}">


            <c:out value="${message.accountIdSent}"/> : <c:out value="${message.text}"/><br>


        </c:forEach>

</div>


<%--<div class="conmessage">--%>


<%--    <form class="formmessage" method="post" action="">--%>
<%--        <div class="sentmessage">--%>

<%--            <input class="inputmessage" type="text" required placeholder="Message..." name="message"/>--%>
<%--        </div>--%>

<%--        <div class="button-sent">--%>
<%--            <button class="b" type="submit">sent</button>--%>
<%--        </div>--%>

<%--    </form>--%>

<%--</div>--%>








<script src="../../resources/assets/js/vendor/jquery.min.js"></script>
<script src="../../resources/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<script src="../../resources/assets/js/vendor/bootstrap.min.js"></script>
<script src="../../resources/assets/js/vendor/popper.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery-mean-menu.min.js"></script>
<script src="../../resources/assets/js/vendor/owl.carousel.min.js"></script>
<script src="../../resources/assets/js/vendor/isotope.pkgd.min.js"></script>
<script src="../../resources/assets/js/countdown.js"></script>
<script src="../../resources/assets/js/vendor/jquery.nice-number.js"></script>
<script src="../../resources/assets/js/vendor/slick.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="../../resources/assets/js/vendor/wow-1.3.0.min.js"></script>
<script src="../../resources/assets/js/main.js"></script>
</body>
</html>

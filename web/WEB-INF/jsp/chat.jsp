<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 27.10.2022
  Time: 20:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <script defer type="text/javascript" src="../../resources/jquery-3.6.1.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
    <script src="https://malsup.github.io/jquery.form.js"></script>

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <!-- title -->


    <!-- stylesheets -->
    <link rel="stylesheet" href="../../resources/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/slick.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/magnific-popup.css">
    <link rel="stylesheet" href="../../resources/assets/css/vendor/normalize.css">
    <link rel="stylesheet" href="../../resources/assets/css/mean-menu.css">
    <link rel="stylesheet" href="../../resources/assets/css/default.css">
    <link rel="stylesheet" href="../../resources/assets/css/style.css">
    <link rel="stylesheet" href="../../resources/assets/css/style-chat.css">

    <title>Messenger</title>
    <style>
        /*body {*/
        /*    background: #ffffff;*/
        /*    margin-top: 10px;*/
        /*    display: block;*/
        /*    margin-left: auto;*/
        /*    margin-right: auto*/
        /*}*/

        /*.chat-content {*/
        /*    position: relative;*/
        /*    display: block;*/
        /*    float: right;*/
        /*    padding: 8px 15px;*/
        /*    margin: 0 20px 10px 0;*/
        /*    clear: both;*/
        /*    color: #fff;*/
        /*    background-color: #ff4040;*/
        /*    border-radius: 4px;*/
        /*    -webkit-box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.37);*/
        /*    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.37);*/
        /*}*/

        /*.chat-content:before {*/
        /*    position: absolute;*/
        /*    top: 10px;*/
        /*    right: -10px;*/
        /*    width: 0;*/
        /*    height: 0;*/
        /*    content: '';*/

        /*    border: 5px solid transparent;*/
        /*    border-left-color: #ff4040*/
        /*}*/

        /*.eight {*/
        /*    position: relative;*/
        /*    left: 5px;*/
        /*    bottom: 10px;*/

        /*}*/

        /*.ten {*/
        /*    display: block;*/
        /*    margin-left: auto;*/
        /*    margin-right: auto*/
        /*}*/
    </style>
</head>
<body>
<header>
    <div class="header-area header-9 header-resposive pt-30 ">

        <div class="header-menu mt-15 pt-25 pb-20">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 col-md-10">
                        <div class="main-menu ">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="/home_page">Главная страница</a></li>


                                    <li><a href="/account/products">Мои товары</a>

                                    </li>
                                    <li><a href="/account/orders">Мои заказы</a></li>
                                    <li><a href="/account/chats">Мои чаты</a></li>

                                </ul>
                            </nav>

                        </div>
                    </div>
                    <div class="col-xl-2 col-md-2">
                        <div class="site-info d-flex justify-content-end">
                            <div class="search position-relative mr-15 ">
                                <a href="/logout">Выйти</a>

                            </div>


                        </div>
                    </div>

                </div>
                <div class="mobile-menu"></div>
            </div>
        </div>


    </div>
</header>
<div class="container bootstrap snippets bootdeys">

    <div class="header-area header-9 header-resposive pt-30 ">

        <div class="col-md-7 col-xs-12 col-md-offset-2 ten">
            <!-- Panel Chat -->

            <div class="panel ten" id="chat">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="icon wb-chat-text" aria-hidden="true"></i> Chat
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="chats">
                        <c:if test="${requestScope.messages!=null}">
                            <c:forEach var="message" items="${requestScope.messages}">
                                <c:if test="${message.accountFirstName.equals(sessionScope.account.firstName)}">


                                    <div class="chat">


                                        <div class="chat-body">
                                            <div class="chat-content">
                                                <p><c:out value="${message.text}"/></p>
                                                <time class="chat-time" datetime="2015-07-01T11:37"><c:out
                                                        value="${message.time}"/></time>
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                                <c:if test="${!message.accountFirstName.equals(sessionScope.account.firstName)}">

                                    <div class="chat chat-left">

                                        <div class="chat-body">
                                            <div class="chat-content">
                                                <p><c:out value="${message.text}"/></p>
                                                <time class="chat-time" datetime="2015-07-01T11:39"><c:out
                                                        value="${message.time}"/></time>
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </c:if>
                    </div>
                </div>



                <div class="panel-footer">
                    <form class="" method="post" action="">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Say something" name="message"/>
                            <div class="input-group-btn">
                                <button class="btn btn-first first eight" type="submit">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End Panel Chat -->
        </div>
    </div>
</div>


<script src="../../resources/assets/js/vendor/jquery.min.js"></script>
<script src="../../resources/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<script src="../../resources/assets/js/vendor/bootstrap.min.js"></script>
<script src="../../resources/assets/js/vendor/popper.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery-mean-menu.min.js"></script>
<script src="../../resources/assets/js/vendor/owl.carousel.min.js"></script>
<script src="../../resources/assets/js/vendor/isotope.pkgd.min.js"></script>
<script src="../../resources/assets/js/countdown.js"></script>
<script src="../../resources/assets/js/vendor/jquery.nice-number.js"></script>
<script src="../../resources/assets/js/vendor/slick.min.js"></script>
<script src="../../resources/assets/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="../../resources/assets/js/vendor/wow-1.3.0.min.js"></script>
<script src="../../resources/assets/js/main.js"></script>
</body>
</html>
